﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class ChiTietHoaDon : BaseViewModel
    {
       
        private string _IdThucDon;
        [Key, Column(Order = 1)]
        public string IdThucDon { get => _IdThucDon; set { _IdThucDon = value; OnPropertyChanged(); } }
        private DateTime _IdHoaDon;
        [Key, Column(Order = 2)]
        public DateTime IdHoaDon { get => _IdHoaDon; set { _IdHoaDon = value; OnPropertyChanged(); } }

        private int _TongTien;
        public int TongTien { get => _TongTien; set { _TongTien = value; OnPropertyChanged(); } }
        private int _SoLuong;
        public int SoLuong { get => _SoLuong; set { _SoLuong = value; OnPropertyChanged(); } }
        private int _GiaTien;
        public int GiaTien { get => _GiaTien; set { _GiaTien = value; OnPropertyChanged(); } }
        private ThucDon _ThucDon;
        [ForeignKey("IdThucDon")]
        public virtual ThucDon LoaiNgheNghiep { get => _ThucDon; set { _ThucDon = value; OnPropertyChanged(); } }

        private HoaDon _HoaDon;
        [ForeignKey("IdHoaDon")]
        public virtual HoaDon HoaDon { get => _HoaDon; set { _HoaDon = value; OnPropertyChanged(); } }
    }
}
