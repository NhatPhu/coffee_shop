﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class NguoiDung : BaseViewModel
    {
        [Key]
        public string IdNguoiDung { get; set; }
        private string _IdQuyen;
        public string IdQuyen { get => _IdQuyen; set { _IdQuyen = value; OnPropertyChanged(); } }
        private string _MatKhau;
        public string MatKhau { get => _MatKhau; set { _MatKhau = value; OnPropertyChanged(); } }
        private string _TenNguoiDung;
        public string TenNguoiDung { get => _TenNguoiDung; set { _TenNguoiDung = value; OnPropertyChanged(); } }
        private string _DiaChi;
        public string DiaChi { get => _DiaChi; set { _DiaChi = value; OnPropertyChanged(); } }
        private string _Phone;
        public string Phone { get => _Phone; set { _Phone = value; OnPropertyChanged(); } }
        private string _Email;
        public string Email { get => _Email; set { _Email = value; OnPropertyChanged(); } }
        private Boolean _TrangThai;
        public Boolean TrangThai { get => _TrangThai; set { _TrangThai = value; OnPropertyChanged(); } }
        private Nullable<System.DateTime> _NgayLamViec;
        public Nullable<System.DateTime> NgayLamViec { get => _NgayLamViec; set { _NgayLamViec = value; OnPropertyChanged(); } }
        private Boolean _Check;
        public Boolean Check { get => _Check; set { _Check = value; OnPropertyChanged(); } }

        private Quyen _Quyen;
        [ForeignKey("IdQuyen")]
        public virtual Quyen Quyen { get => _Quyen; set { _Quyen = value; OnPropertyChanged(); } }
        //private ICollection<ThongTinXuat> _ThongTinXuat;
        //public virtual ICollection<ThongTinXuat> ThongTinXuat { get => _ThongTinXuat; set { _ThongTinXuat = value; OnPropertyChanged(); } }
        private ICollection<HoaDon> _HoaDon;
        public virtual ICollection<HoaDon> HoaDon { get => _HoaDon; set { _HoaDon = value; OnPropertyChanged(); } }
    }
}
