﻿using Coffe_Shop.View;
using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class ThucDon : BaseViewModel
    {
       
        private string _IdThucDon;
        
        [Required(ErrorMessage ="Bạn phải nhập mã món")]
        [Key]
        public string IdThucDon { get => _IdThucDon; set { ValidateProperty(value, "IdThucDon"); _IdThucDon = value; OnPropertyChanged();  } }
        private string _TenThucDon;
        [Required(ErrorMessage = "Bạn phải nhập tên món")]
        [StringLength(30, MinimumLength = 5, ErrorMessage = "Tên món phải trên 5 ký tự.")]
        public string TenThucDon { get => _TenThucDon; set { ValidateProperty(value, "TenThucDon"); _TenThucDon = value; OnPropertyChanged();  } }
        private int _GiaTien;
        [Required(ErrorMessage = "Bạn phải nhập giá món")]
        public int GiaTien
        {
            get => _GiaTien;
            set
            {
                IsOnPropertyChangedGiaTien = false;
                //int i;
                //StringToIntValidationRule a = new StringToIntValidationRule();
                
                if (value < 0 || value > 100000)
                    throw new ArgumentException("Giá tiền từ 0 đến 100000.");

                _GiaTien = value;
                OnPropertyChanged();
                IsOnPropertyChangedGiaTien = true;
            }
        }
        public bool IsOnPropertyChangedGiaTien { get; set; }
        private string _TinhTrang;
        public string TinhTrang { get => _TinhTrang; set { _TinhTrang = value; OnPropertyChanged(); } }
        private string _Anh;
        public string Anh { get => _Anh; set { _Anh = value; OnPropertyChanged(); } }
        private int _LanSua;
        public int LanSua { get => _LanSua; set { _LanSua = value; OnPropertyChanged(); } }
        private Boolean _TrangThai;
        public Boolean TrangThai { get => _TrangThai; set { _TrangThai = value; OnPropertyChanged(); } }
        private ICollection<ChiTietHoaDon> _ChiTietHoaDon;
        public virtual ICollection<ChiTietHoaDon> ChiTietHoaDon { get => _ChiTietHoaDon; set { _ChiTietHoaDon = value; OnPropertyChanged(); } }
        private void ValidateProperty<T>(T value, string name)
        {
            //IsOnPropertyChanged = false;
            Validator.ValidateProperty(value, new ValidationContext(this, null, null)
            {
                MemberName = name
            });
        }
       
    }
}
