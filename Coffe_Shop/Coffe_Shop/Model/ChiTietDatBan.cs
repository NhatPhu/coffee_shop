﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class ChiTietDatBan : BaseViewModel
    {
        private string _IdKhachHang;
        [Key, Column(Order = 1)]
        public string IdKhachHang { get => _IdKhachHang; set { _IdKhachHang = value; OnPropertyChanged(); } }
       
        private DateTime _NgayDat;
        [Key, Column(Order = 2)]
        public DateTime NgayDat { get => _NgayDat; set { _NgayDat = value; OnPropertyChanged(); } }
        private string _TrangThai;
        public string TrangThai { get => _TrangThai; set { _TrangThai = value; OnPropertyChanged(); } }
        private int _SoLuong;
        public int SoLuong { get => _SoLuong; set { _SoLuong = value; OnPropertyChanged(); } }
        private KhachHang _KhachHang;
        [ForeignKey("IdKhachHang")]
        public virtual KhachHang KhachHang { get => _KhachHang; set { _KhachHang = value; OnPropertyChanged(); } }
    }

}
