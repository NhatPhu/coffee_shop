﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class HoaDon : BaseViewModel
    {
        [Key, Column(Order = 1)]
        public DateTime IdHoaDon { get; set; }
       
        private string _IdNguoiDung;
        public string IdNguoiDung { get => _IdNguoiDung; set { _IdNguoiDung = value; OnPropertyChanged(); } }
        private string _IdHinhThuc;
        public string IdHinhThuc { get => _IdHinhThuc; set { _IdHinhThuc = value; OnPropertyChanged(); } }
        private string _IdThue;
        public string IdThue { get => _IdThue; set { _IdThue = value; OnPropertyChanged(); } }
        private string _IdKhachHang;
        public string IdKhachHang { get => _IdKhachHang; set { _IdKhachHang = value; OnPropertyChanged(); } }
        //private DateTime _NgayLap;

        //public DateTime NgayLap { get => _NgayLap; set { _NgayLap = value; OnPropertyChanged(); } }
        private int _TongTien;
        public int TongTien { get => _TongTien; set { _TongTien = value; OnPropertyChanged(); } }
        private string _TrangThai;
        public string TrangThai { get => _TrangThai; set { _TrangThai = value; OnPropertyChanged(); } }
        private NguoiDung _NguoiDung;
        [ForeignKey("IdNguoiDung")]
        public virtual NguoiDung NguoiDung { get => _NguoiDung; set { _NguoiDung = value; OnPropertyChanged(); } }
        private KhachHang _KhachHang;
        [ForeignKey("IdKhachHang")]
        public virtual KhachHang KhachHang { get => _KhachHang; set { _KhachHang = value; OnPropertyChanged(); } }
        private HinhThucThanhToan _HinhThucThanhToan;
        [ForeignKey("IdHinhThuc")]
        public virtual HinhThucThanhToan HinhThucThanhToan { get => _HinhThucThanhToan; set { _HinhThucThanhToan = value; OnPropertyChanged(); } }
        private Thue _Thue;
        [ForeignKey("IdThue")]
        public virtual Thue Thue { get => _Thue; set { _Thue = value; OnPropertyChanged(); } }
        private ICollection<ChiTietHoaDon> _ChiTietHoaDon;
        public virtual ICollection<ChiTietHoaDon> ChiTietHoaDon { get => _ChiTietHoaDon; set { _ChiTietHoaDon = value; OnPropertyChanged(); } }
    }
}
