﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class KhachHang : BaseViewModel
    {
        [Key]
        public string IdKhachHang { get; set; }
        private string _TenKhachHang;
        public string TenKhachHang { get => _TenKhachHang; set { _TenKhachHang = value; OnPropertyChanged(); } }
        private string _SDT;
        public string SDT { get => _SDT; set { _SDT = value; OnPropertyChanged(); } }
        private string _DiaChi;
        public string DiaChi { get => _DiaChi; set { _DiaChi = value; OnPropertyChanged(); } }
        private Boolean _TrangThai;
        public Boolean TrangThai { get => _TrangThai; set { _TrangThai = value; OnPropertyChanged(); } }
        private ICollection<HoaDon> _HoaDon;
        public virtual ICollection<HoaDon> HoaDon { get => _HoaDon; set { _HoaDon = value; OnPropertyChanged(); } }
        private ICollection<ChiTietDatBan> _ChiTietDatBan;
        public virtual ICollection<ChiTietDatBan> ChiTietDatBan { get => _ChiTietDatBan; set { _ChiTietDatBan = value; OnPropertyChanged(); } }
    }
}
