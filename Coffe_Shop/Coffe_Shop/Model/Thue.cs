﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class Thue : BaseViewModel
    {
        [Key]
        public string IdThue { get; set; }
        private string _TenThue;
        public string TenThue { get => _TenThue; set { _TenThue = value; OnPropertyChanged(); } }
        private int _TiGia;
        public int TiGia { get => _TiGia; set { _TiGia = value; OnPropertyChanged(); } }
        private ICollection<HoaDon> _HoaDon;
        public virtual ICollection<HoaDon> HoaDon { get => _HoaDon; set { _HoaDon = value; OnPropertyChanged(); } }
    }
}
