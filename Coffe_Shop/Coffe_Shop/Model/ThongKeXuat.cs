﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class ThongKeXuat : ViewModel.BaseViewModel
    {
        public int _STT { get; set; }
        public int STT { get => _STT; set { _STT = value; OnPropertyChanged(); } }
        public int? _Count { get; set; }
        public int? Count { get => _Count; set { _Count = value; OnPropertyChanged(); } }
        private string _Object;
        public string Object { get => _Object; set { _Object = value; OnPropertyChanged(); } }

        private string _Unit;
        public string Unit { get => _Unit; set { _Unit = value; OnPropertyChanged(); } }
        private double? _Tien;
        public double? Tien { get => _Tien; set { _Tien = value; OnPropertyChanged(); } }
    }
}
