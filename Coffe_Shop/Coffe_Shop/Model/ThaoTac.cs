﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class ThaoTac: BaseViewModel
    {
        private string _IdThaoTac;
        [Key]
        public string IdThaoTac { get => _IdThaoTac; set { _IdThaoTac = value; OnPropertyChanged(); } }
        private Boolean _Check;
        public Boolean Check { get => _Check; set { _Check = value; OnPropertyChanged(); } }
       
        private ICollection<ChiTietThaoTac> _ChiTietThaoTac;
        public virtual ICollection<ChiTietThaoTac> ChiTietThaoTac { get => _ChiTietThaoTac; set { _ChiTietThaoTac = value; OnPropertyChanged(); } }
    }
}
