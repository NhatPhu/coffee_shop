﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class Ban : BaseViewModel
    {
        private string _IdBan;
        [Key]
        public string IdBan { get => _IdBan; set { _IdBan = value; OnPropertyChanged(); } }
        private Boolean _TinhTrang;
        public Boolean TinhTrang { get => _TinhTrang; set { _TinhTrang = value; OnPropertyChanged(); } }
        private Boolean _TrangThai;
        public Boolean TrangThai { get => _TrangThai; set { _TrangThai = value; OnPropertyChanged(); } }
        private string _Mau;
        public string Mau { get => _Mau; set { _Mau = value; OnPropertyChanged(); } }
    }
}
