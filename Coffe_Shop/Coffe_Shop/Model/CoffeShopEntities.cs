﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class CoffeShopEntities : DbContext
    {
        public DbSet<ThaoTac> DsThaoTac { get; set; }
        public DbSet<Quyen> DsQuyen { get; set; }
        public DbSet<ChiTietThaoTac> DsChiTietThaoTac { get; set; }
        public DbSet<ThucDon> DsThucDon { get; set; }
        public DbSet<NguoiDung> DsNguoiDung { get; set; }
        public DbSet<HoaDon> DsHoaDon { get; set; }
        public DbSet<KhachHang> DsKhachHang { get; set; }
        public DbSet<Thue> DsThue { get; set; }
        public DbSet<HinhThucThanhToan> DsHinhThucThanhToan { get; set; }
        public DbSet<Ban> DsBan { get; set; }
        public DbSet<ChiTietDatBan> DsChiTietDatBan { get; set; }
        public DbSet<ChiTietHoaDon> DsChiTietHoaDon { get; set; }
        //public DbSet<ThanhVien> DsThanhVien { get; set; }
        //public DbSet<LoaiNgheNghiep> DsNgheNghiep { get; set; }
        //public DbSet<LoaiQueQuan> DsQueQuan { get; set; }
        //public DbSet<LoaiQuanHe> DsQuanHe { get; set; }
        //public DbSet<LoaiThanhTich> DsThanhTich { get; set; }
        //public DbSet<LoaiNguyenNhanMat> DsNguyenNhanMat { get; set; }
        //public DbSet<LoaiDiaDiemChon> DsDiaDiemChon { get; set; }
        //public DbSet<ChiTietQuanHe> DsChiTietQuanHe { get; set; }
        //public DbSet<ChiTietThanhTich> DsChiTietThanhTich { get; set; }
        //public DbSet<ChiTietMat> DsChiTietMat { get; set; }
    }
}
