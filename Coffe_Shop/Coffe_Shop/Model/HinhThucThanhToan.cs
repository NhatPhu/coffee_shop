﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class HinhThucThanhToan : BaseViewModel
    {

        [Key]
        public string IdHinhThuc { get; set; }
        private string _TenHinhThuc;
        public string TenHinhThuc { get => _TenHinhThuc; set { _TenHinhThuc = value; OnPropertyChanged(); } }

        private ICollection<HoaDon> _HoaDon;
        public virtual ICollection<HoaDon> HoaDon { get => _HoaDon; set { _HoaDon = value; OnPropertyChanged(); } }

    }
}
