﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class ChiTietThaoTac : BaseViewModel
    {
        private string _IdQuyen;
        [Key, Column(Order = 1)]
        public string IdQuyen { get => _IdQuyen; set { _IdQuyen = value; OnPropertyChanged(); } }
        private string _IdThaoTac;
        [Key, Column(Order = 2)]
        public string IdThaoTac { get => _IdThaoTac; set { _IdThaoTac = value; OnPropertyChanged(); } }

        private Quyen _Quyen;
        [ForeignKey("IdQuyen")]
        public virtual Quyen Quyen { get => _Quyen; set { _Quyen = value; OnPropertyChanged(); } }

        private ThaoTac _ThaoTac;
        [ForeignKey("IdThaoTac")]
        public virtual ThaoTac ThaoTac { get => _ThaoTac; set { _ThaoTac = value; OnPropertyChanged(); } }
    }
}
