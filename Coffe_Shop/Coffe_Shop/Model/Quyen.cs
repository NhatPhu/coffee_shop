﻿using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.Model
{
    public class Quyen : BaseViewModel
    {
        private string _IdQuyen;
        [Key]
        public string IdQuyen { get => _IdQuyen; set { _IdQuyen = value; OnPropertyChanged(); } }
        
        //private string _GhiChu;
        //public string GhiChu { get => _GhiChu; set { _GhiChu = value; OnPropertyChanged(); } }
       
        private Boolean _Check;
        public Boolean Check { get => _Check; set { _Check = value; OnPropertyChanged(); } }
        private ICollection<NguoiDung> _NguoiDung;
        public virtual ICollection<NguoiDung> NguoiDung { get => _NguoiDung; set { _NguoiDung = value; OnPropertyChanged(); } }
        private ICollection<ChiTietThaoTac> _ChiTietThaoTac;
        public virtual ICollection<ChiTietThaoTac> ChiTietThaoTac { get => _ChiTietThaoTac; set { _ChiTietThaoTac = value; OnPropertyChanged(); } }
    }
}
