﻿using Coffe_Shop.Model;
using Coffe_Shop.ViewModel;
using Dragablz;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class DanhMucViewModel : BaseViewModel
    {

        private ObservableCollection<Thue> _Thue;
        public ObservableCollection<Thue> Thue { get => _Thue; set { _Thue = value; OnPropertyChanged(); } }
        private ObservableCollection<HinhThucThanhToan> _HinhThuc;
        public ObservableCollection<HinhThucThanhToan> HinhThuc { get => _HinhThuc; set { _HinhThuc = value; OnPropertyChanged(); } }

        private Thue _SelectedThue;
        public Thue SelectedThue { get => _SelectedThue; set { _SelectedThue = value; OnPropertyChanged(); } }
        private HinhThucThanhToan _SelectedHinhThuc;
        public HinhThucThanhToan SelectedHinhThuc { get => _SelectedHinhThuc; set { _SelectedHinhThuc = value; OnPropertyChanged(); } }
        private string _IdThue;
        public string IdThue { get => _IdThue; set { _IdThue = value; OnPropertyChanged(); } }
        private string _TenThue;
        public string TenThue { get => _TenThue; set { _TenThue = value; OnPropertyChanged(); } }
        private string _IdHinhThuc;
        public string IdHinhThuc { get => _IdHinhThuc; set { _IdHinhThuc = value; OnPropertyChanged(); } }
        private string _TenHinhThuc;
        public string TenHinhThuc { get => _TenHinhThuc; set { _TenHinhThuc = value; OnPropertyChanged(); } }
        private int _SelectedIndex;
        public int SelectedIndex
        {
            get => _SelectedIndex;
            set
            {
                _SelectedIndex = value;
                OnPropertyChanged();
                if (SelectedIndex >= 0)
                {
                    if (SelectedIndex == 0 && IsAddThue == true)
                    {
                        DanhMucTitle = "Danh mục Thuế: Thêm thuế mới";
                    }
                    else if (SelectedIndex == 1 && IsAddHinhThuc == true)
                    {
                        DanhMucTitle = "Danh mục Hình thức: Thêm hình thức mới";
                    }
                    else if (SelectedIndex == 1 && IsAddHinhThuc == false)
                    {
                        DanhMucTitle = "Danh mục Hình thức thanh toán";
                    }
                    else if (SelectedIndex == 0 && IsAddThue == false)
                    {
                        DanhMucTitle = "Danh mục Thuế";
                    }
                }
            }
        }
        

        //**************************************************

        //private string _TenQueQuan { get; set; }
        //public string TenQueQuan { get => _TenQueQuan; set { _TenQueQuan = value; OnPropertyChanged(); } }
        private string _ToolTip { get; set; }
        public string ToolTip { get => _ToolTip; set { _ToolTip = value; OnPropertyChanged(); } }
        public ICommand AddNewCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        //public ICommand SelectionChangedCommand { get; set; }
        private string _DanhMucTitle; 
        public string DanhMucTitle { get => _DanhMucTitle; set { _DanhMucTitle = value; OnPropertyChanged(); } }
        private Boolean _IsAddThue; 
        public Boolean IsAddThue { get => _IsAddThue; set { _IsAddThue = value; OnPropertyChanged(); } }
        private Boolean _IsAddHinhThuc;
        public Boolean IsAddHinhThuc { get => _IsAddHinhThuc; set { _IsAddHinhThuc = value; OnPropertyChanged(); } }

        public DanhMucViewModel()
        {
            //IsAdd = true;
            Thue = new ObservableCollection<Thue>(DataProvider.Ins.DB.DsThue);
            HinhThuc = new ObservableCollection<HinhThucThanhToan>(DataProvider.Ins.DB.DsHinhThucThanhToan);
            DanhMucTitle = "Danh Mục";
            // SelectionChangedCommand = new RelayCommand<object>((p) => { return true; }, (p) => { Id = ""; Ten = ""; });
            AddNewCommand = new RelayCommand<object>((p) =>
            {
                if (SelectedIndex == 0 && IsAddThue == false)
                {
                    return true;
                }
                else if (SelectedIndex == 1 && IsAddHinhThuc == false)
                { 
                   return true;
                }
                else return true;
            }, (p) =>
            {
                //IsAdd = true;
                if (SelectedIndex == 0)
                {
                    DanhMucTitle = "Danh mục Thuế: Thêm thuế mới";
                    IdThue = (int.Parse(Thue[Thue.Count - 1].IdThue) + 1).ToString();
                    TenThue = "";
                    SelectedThue = null;
                    IsAddThue = true;
                }
                else if (SelectedIndex == 1)
                {
                    DanhMucTitle = "Danh mục Thuế: Thêm hình thức mới";
                    IdHinhThuc = (int.Parse(HinhThuc[HinhThuc.Count - 1].IdHinhThuc) + 1).ToString();
                    TenHinhThuc = "";
                    SelectedHinhThuc = null;
                    IsAddHinhThuc = true;
                }
            });
            SaveCommand = new RelayCommand<object>((p) =>
            {
                if (SelectedIndex == 0)
                {
                    if (TenThue != "" && TenThue != null )
                        return true;
                    else return false;
                }
                else if (SelectedIndex == 1)
                {
                    if (TenHinhThuc != "" && TenHinhThuc != null)
                        return true;
                    else return false;
                }
                else return true;
            }, (p) =>
            {
                if (SelectedIndex == 0)
                {
                    if (IsAddThue && TenThue != "")
                    {
                        Thue.Add(new Thue() { IdThue = IdThue, TenThue = TenThue });
                        DataProvider.Ins.DB.DsThue.Add(new Thue() { IdThue = IdThue, TenThue = TenThue });
                        DataProvider.Ins.DB.SaveChanges();
                        IsAddThue = false;
                        IdThue = "";
                        TenThue = "";
                    }
                }
                else if (SelectedIndex == 1)
                {
                    if (IsAddHinhThuc && TenHinhThuc != null)
                    {
                        HinhThuc.Add(new HinhThucThanhToan() { IdHinhThuc = IdHinhThuc, TenHinhThuc = TenHinhThuc });
                        DataProvider.Ins.DB.SaveChanges();
                        DataProvider.Ins.DB.DsHinhThucThanhToan.Add(new HinhThucThanhToan() { IdHinhThuc = IdHinhThuc, TenHinhThuc = TenHinhThuc });
                        DataProvider.Ins.DB.SaveChanges();
                        IsAddHinhThuc = false;
                        IdHinhThuc = "";
                        TenHinhThuc = "";
                    }
                }
            });
        }
    }
}
