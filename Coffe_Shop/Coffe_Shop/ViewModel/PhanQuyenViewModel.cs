﻿using Coffe_Shop.Model;
using Coffe_Shop.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class PhanQuyenViewModel : BaseViewModel
    {
        private ObservableCollection<ThaoTac> _ThaoTacConLai;
        public ObservableCollection<ThaoTac> ThaoTacConLai { get => _ThaoTacConLai; set { _ThaoTacConLai = value; OnPropertyChanged(); } }
        private ObservableCollection<ThaoTac> _ThaoTacDcChon;
        public ObservableCollection<ThaoTac> ThaoTacDcChon { get => _ThaoTacDcChon; set { _ThaoTacDcChon = value; OnPropertyChanged(); } }
        private ObservableCollection<Quyen> _Quyen;
        public ObservableCollection<Quyen> Quyen { get => _Quyen; set { _Quyen = value; OnPropertyChanged(); } }
        public ChiTietThaoTac chiTietThaoTac;
        private string _NhomQuyenMoi;
        public string NhomQuyenMoi { get => _NhomQuyenMoi; set { _NhomQuyenMoi = value; OnPropertyChanged(); } }

        private Quyen _SelectedQuyen;
        public Quyen SelectedQuyen
        {
            get => _SelectedQuyen;
            set
            {
                _SelectedQuyen = value;
                OnPropertyChanged();
                if(SelectedQuyen != null)
                {

                    var checkNullQuyen = DataProvider.Ins.DB.DsChiTietThaoTac.Where(p => p.IdQuyen == SelectedQuyen.IdQuyen);
                    if(checkNullQuyen != null && checkNullQuyen.Count() > 0)
                    {
                        var inputInfo2 = from e in DataProvider.Ins.DB.DsThaoTac
                                         from o in DataProvider.Ins.DB.DsChiTietThaoTac
                                         where e.IdThaoTac == o.IdThaoTac && o.IdQuyen == SelectedQuyen.IdQuyen
                                         select e;
                        ThaoTacDcChon = new ObservableCollection<ThaoTac>(inputInfo2);
                        var inputInfo = from e in DataProvider.Ins.DB.DsThaoTac
                                        where !inputInfo2.Contains(e)
                                        select e;
                        ThaoTacConLai = new ObservableCollection<ThaoTac>(inputInfo);
                    }
                    else
                    {
                        ThaoTacConLai = new ObservableCollection<ThaoTac>(DataProvider.Ins.DB.DsThaoTac);
                        ThaoTacDcChon = new ObservableCollection<ThaoTac>();
                    }
                }
            }
        }

        public ICommand LoadedWindowCommand { get; set; } //Bài Coffe Shop
        public ICommand ChonCommand { get; set; } //Bài Coffy Shop
        public ICommand ChonCommand2 { get; set; } //Bài Coffy Shop
        public ICommand SaveCommand { get; set; }
        public ICommand ThemQuyenCommand { get; set; }
        public PhanQuyenViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                Quyen = new ObservableCollection<Quyen>(DataProvider.Ins.DB.DsQuyen.Where(q=>q.IdQuyen != "Admin"));
                
            });
            ChonCommand = new RelayCommand<ThaoTac>((p) => { return true; }, (p) => {
                ThaoTacDcChon.Remove(p);
                ThaoTacConLai.Add(p);
            });
            ChonCommand2 = new RelayCommand<ThaoTac>((p) => { return true; }, (p) => {
                ThaoTacDcChon.Add(p);
                ThaoTacConLai.Remove(p);
            });
            SaveCommand = new RelayCommand<ThaoTac>((p) =>
            {
                return true;
            }, (p) => 
            {
                var x = DataProvider.Ins.DB.DsChiTietThaoTac.Where(q => q.IdQuyen == SelectedQuyen.IdQuyen);
                DataProvider.Ins.DB.DsChiTietThaoTac.RemoveRange(x);
                if(ThaoTacDcChon.Count() > 0 && ThaoTacDcChon != null)
                {
                    foreach (ThaoTac i in ThaoTacDcChon)
                    {
                        DataProvider.Ins.DB.DsChiTietThaoTac.Add(new ChiTietThaoTac() { IdQuyen = SelectedQuyen.IdQuyen, IdThaoTac = i.IdThaoTac });
                    }
                }
                DataProvider.Ins.DB.SaveChanges();
            });
            ThemQuyenCommand = new RelayCommand<ThaoTac>((p) =>
            {
                var x = Quyen.Where(q => q.IdQuyen == NhomQuyenMoi).SingleOrDefault();
                if (NhomQuyenMoi != null && x == null)
                    return true;
                else return false;
            }, (p) =>
            {
                Quyen.Add(new Quyen() { IdQuyen = NhomQuyenMoi });
                DataProvider.Ins.DB.DsQuyen.Add(new Quyen() { IdQuyen = NhomQuyenMoi });
                DataProvider.Ins.DB.SaveChanges();
            });
        }
    }
}
