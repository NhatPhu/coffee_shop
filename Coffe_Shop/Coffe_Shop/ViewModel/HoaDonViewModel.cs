﻿using Coffe_Shop.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class HoaDonViewModel : BaseViewModel
    {

        private ObservableCollection<HoaDon> _HoaDon;
        public ObservableCollection<HoaDon> HoaDon { get => _HoaDon; set { _HoaDon = value; OnPropertyChanged(); } }//ThongKeXuatList
        private ObservableCollection<ViewDsChiTietHoaDon> _ViewDsChiTietHoaDon;
        public ObservableCollection<ViewDsChiTietHoaDon> ViewDsChiTietHoaDons { get => _ViewDsChiTietHoaDon; set { _ViewDsChiTietHoaDon = value; OnPropertyChanged(); } }

        private ObservableCollection<ThongKeXuat> _ThongKeXuatList;
        public ObservableCollection<ThongKeXuat> ThongKeXuatList { get => _ThongKeXuatList; set { _ThongKeXuatList = value; OnPropertyChanged(); } }//ThongKeXuatList
        private string _NgayLap;
        public string NgayLap { get => _NgayLap; set { _NgayLap = value; OnPropertyChanged(); } }
        private string _TenNhanVien;
        public string TenNhanVien { get => _TenNhanVien; set { _TenNhanVien = value; OnPropertyChanged(); } }
        private string _TenThue;
        public string TenThue { get => _TenThue; set { _TenThue = value; OnPropertyChanged(); } }
        private string _TenHinhThuc;
        public string TenHinhThuc { get => _TenHinhThuc; set { _TenHinhThuc = value; OnPropertyChanged(); } }
        private string _Search;
        public string Search { get => _Search; set { _Search = value; OnPropertyChanged(); } }
        private int _TongTien;
        public int TongTien { get => _TongTien; set { _TongTien = value; OnPropertyChanged(); } }

        private DateTime _InputDateOutput;
        public DateTime InputDateOutput { get => _InputDateOutput; set { _InputDateOutput = value; OnPropertyChanged(); } }
        private DateTime _InputDateInput;
        public DateTime InputDateInput { get => _InputDateInput; set { _InputDateInput = value; OnPropertyChanged(); } }
        private HoaDon _SelectedHoaDon;
        public HoaDon SelectedHoaDon
        {
            get => _SelectedHoaDon;
            set
            {
                _SelectedHoaDon = value;
                OnPropertyChanged();
                if(SelectedHoaDon != null)
                {
                    NgayLap = SelectedHoaDon.IdHoaDon.ToString();
                    TenNhanVien = DataProvider.Ins.DB.DsNguoiDung.Where(p => p.IdNguoiDung == SelectedHoaDon.IdNguoiDung).SingleOrDefault().TenNguoiDung;
                    TenHinhThuc = DataProvider.Ins.DB.DsHinhThucThanhToan.Where(p => p.IdHinhThuc == SelectedHoaDon.IdHinhThuc).SingleOrDefault().TenHinhThuc;
                    TenThue = DataProvider.Ins.DB.DsThue.Where(p => p.IdThue == SelectedHoaDon.IdThue).SingleOrDefault().TenThue;
                    var a = DataProvider.Ins.DB.DsChiTietHoaDon.Where(p => p.IdHoaDon == SelectedHoaDon.IdHoaDon).ToList();
                    
                    ViewDsChiTietHoaDons = new ObservableCollection<ViewDsChiTietHoaDon>();
                    foreach(ChiTietHoaDon x in a)
                    {
                        var i = DataProvider.Ins.DB.DsThucDon.Where(p => p.IdThucDon == x.IdThucDon).Single().TenThucDon;
                        ViewDsChiTietHoaDons.Add(new ViewDsChiTietHoaDon() { Ten = i, SoLuong = x.SoLuong, TongTien = x.TongTien, GiaTien = x.GiaTien });
                    }
                }
            }
        }
        public ICommand LoadedWindowCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand NguyenNhanMatCommand { get; set; }
        public ICommand DiaDiemChonCommand { get; set; }
        public ICommand FindInputCommand { get; set; }

        private void ThongKeHomNay()
        {
            int STTNhap = 0;
            var inputInfo = from e in DataProvider.Ins.DB.DsChiTietHoaDon
                            from o in DataProvider.Ins.DB.DsThucDon
                            where e.IdThucDon == o.IdThucDon && e.IdHoaDon < DateTime.Now && e.IdHoaDon >= DateTime.Today
                            select new
                            {
                                soluong = e.SoLuong,
                                ten = o.TenThucDon,
                                tien = e.SoLuong * e.GiaTien,
                                dv = "Ly"
                            };
            var Objects = DataProvider.Ins.DB.DsThucDon;
            foreach (var x in Objects)
            {

                ThongKeXuat thongKeNhap = new ThongKeXuat();

                var info = inputInfo.Where(q => q.ten == x.TenThucDon);
                if (info != null && info.Count() > 0)
                {
                    thongKeNhap.Tien = info.Sum(e => e.tien);
                    thongKeNhap.Count = info.Sum(e => e.soluong);
                    thongKeNhap.STT = STTNhap++;

                    thongKeNhap.Object = info.ToList()[0].ten;
                    thongKeNhap.Unit = "Ly";
                    ThongKeXuatList.Add(thongKeNhap);
                }
            }
            TongTien = inputInfo.Sum(e => e.tien);
        }
        public HoaDonViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) =>
            {
                HoaDon = new ObservableCollection<HoaDon>(DataProvider.Ins.DB.DsHoaDon);
                ViewDsChiTietHoaDons = new ObservableCollection<ViewDsChiTietHoaDon>();
                ThongKeXuatList = new ObservableCollection<ThongKeXuat>();
                InputDateInput = DateTime.Today;
                InputDateOutput = DateTime.Today;
                ThongKeHomNay();
            });
            FindInputCommand = new RelayCommand<object>((p) => { if (InputDateInput != null && InputDateOutput != null) return true; return false; },
           (p) =>
           {
               
               
               
               if (InputDateInput != InputDateOutput)
               {
                   TongTien = 0;
                   if (InputDateOutput == DateTime.Today)
                       InputDateOutput = DateTime.Now;

                   var Objects = DataProvider.Ins.DB.DsThucDon;
                   ThongKeXuatList = new ObservableCollection<ThongKeXuat>();
                   var inputInfo = from e in DataProvider.Ins.DB.DsChiTietHoaDon
                                   from o in DataProvider.Ins.DB.DsThucDon
                                   where e.IdThucDon == o.IdThucDon && e.IdHoaDon >= InputDateInput && e.IdHoaDon <= InputDateOutput
                                   select new
                                   {
                                       soluong = e.SoLuong,
                                       ten = o.TenThucDon,
                                       tien = e.SoLuong * e.GiaTien,
                                       dv = "Ly"
                                   };
                   int STTNhap = 0;
                   foreach (var x in Objects)
                   {
                       ThongKeXuat thongKeNhap = new ThongKeXuat();

                       var info = inputInfo.Where(q => q.ten == x.TenThucDon);
                       if (info != null && info.Count() > 0)
                       {
                           thongKeNhap.Tien = info.Sum(e => e.tien);
                           thongKeNhap.Count = info.Sum(e => e.soluong);
                           thongKeNhap.STT = STTNhap++;

                           thongKeNhap.Object = info.ToList()[0].ten;
                           thongKeNhap.Unit = info.ToList()[0].dv;
                           ThongKeXuatList.Add(thongKeNhap);
                       }
                   }
                   TongTien = inputInfo.Sum(e => e.tien);
               }
               Search = "Từ " + InputDateInput.ToString("dd/MM/yyyy") + " đến " + InputDateOutput.ToString("dd/MM/yyyy");
           });
            EditCommand = new RelayCommand<Window>((p) =>
            {

                return true;

            }, (p) =>
            {

            });
            DeleteCommand = new RelayCommand<Window>((p) =>
            {
                if (SelectedHoaDon.TrangThai == "Đã Xóa")
                    return false;
                else return true;
            }, (p) =>
            {
               foreach(HoaDon x in HoaDon)
                {
                    if(x.IdHoaDon == SelectedHoaDon.IdHoaDon)
                    {
                        x.TrangThai = "Đã Xóa";
                        DataProvider.Ins.DB.SaveChanges();
                        break;
                    }
                }
            });
            DiaDiemChonCommand = new RelayCommand<object>((p) => { return true; }, (p) =>
            {

            });
        }

        public class ViewDsChiTietHoaDon : BaseViewModel
        {
            public string Ten { get; set; }
            public int SoLuong { get; set; }
            public int TongTien { get; set; }
            public int GiaTien { get; set; }
        }
    }
}
