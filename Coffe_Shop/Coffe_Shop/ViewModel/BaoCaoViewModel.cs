﻿//using CayGiaPha.Model;
//using CayGiaPha.ViewModel;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Windows;
//using System.Windows.Input;

//namespace CayGiaPha.ViewModel
//{
//    public class BaoCaoViewModel : BaseViewModel
//    {
//        private ObservableCollection<ThongKeTangGiamThanhVien> _ThongKeTangGiamThanhVien;
//        public ObservableCollection<ThongKeTangGiamThanhVien> ThongKeTangGiamThanhVien { get => _ThongKeTangGiamThanhVien; set { _ThongKeTangGiamThanhVien = value; OnPropertyChanged(); } }
//        private ObservableCollection<ThongKeThanhTich> _ThongKeThanhTich;
//        public ObservableCollection<ThongKeThanhTich> ThongKeThanhTich { get => _ThongKeThanhTich; set { _ThongKeThanhTich = value; OnPropertyChanged(); } }
//        private ObservableCollection<ChiTietMat> _ChiTietMat;
//        public ObservableCollection<ChiTietMat> ChiTietMat { get => _ChiTietMat; set { _ChiTietMat = value; OnPropertyChanged(); } }
//        private ObservableCollection<ChiTietQuanHe> _ChiTietQuanHe;
//        public ObservableCollection<ChiTietQuanHe> ChiTietQuanHe { get => _ChiTietQuanHe; set { _ChiTietQuanHe = value; OnPropertyChanged(); } }
//        private ObservableCollection<ChiTietThanhTich> _ChiTietThanhTich;
//        public ObservableCollection<ChiTietThanhTich> ChiTietThanhTich { get => _ChiTietThanhTich; set { _ChiTietThanhTich = value; OnPropertyChanged(); } }
//        private ObservableCollection<LoaiThanhTich> _LoaiThanhTich;
//        public ObservableCollection<LoaiThanhTich> LoaiThanhTich { get => _LoaiThanhTich; set { _LoaiThanhTich = value; OnPropertyChanged(); } }
//        private DateTime _InputDateInput;
//        public DateTime InputDateInput { get => _InputDateInput; set { _InputDateInput = value; OnPropertyChanged(); } }

//        private DateTime _InputDateOutput;
//        public DateTime InputDateOutput { get => _InputDateOutput; set { _InputDateOutput = value; OnPropertyChanged(); } }
//        public ICommand FindInputCommand { get; set; }
//        public ICommand LoadedWindowCommand { get; set; }
//        public BaoCaoViewModel()
//        {
//            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) =>
//            {
                
//                ChiTietMat = new ObservableCollection<ChiTietMat>(DataProvider.Ins.DB.DsChiTietMat);
//                ChiTietQuanHe = new ObservableCollection<ChiTietQuanHe>(DataProvider.Ins.DB.DsChiTietQuanHe);
//                ChiTietThanhTich = new ObservableCollection<ChiTietThanhTich>(DataProvider.Ins.DB.DsChiTietThanhTich);
//                LoaiThanhTich = new ObservableCollection<LoaiThanhTich>(DataProvider.Ins.DB.DsThanhTich);
//                InputDateInput = DateTime.Today;
//                InputDateOutput = DateTime.Now;
//            });
//            FindInputCommand = new RelayCommand<Window>((p) => 
//            {
//                if(InputDateOutput.Year < InputDateInput.Year)
//                return false;
//                return true;
//            }, (p) =>
//            {
//                ThongKeThanhTich = new ObservableCollection<ThongKeThanhTich>();
//                ThongKeTangGiamThanhVien = new ObservableCollection<ThongKeTangGiamThanhVien>();
//                var dem = 0;
//                for (int i = InputDateInput.Year; i <= InputDateOutput.Year; i++)
//                {
//                    dem++;
//                    var ctm = ChiTietMat.Where(q => q.NgayNhap.Year == i).Count();
//                    var ctqhs = ChiTietQuanHe.Where(q => q.NgayNhap.Year == i && q.IdQuanHe == 1).Count();
//                    var ctqhvc = ChiTietQuanHe.Where(q => q.NgayNhap.Year == i && q.IdQuanHe == 2).Count();
//                    ThongKeTangGiamThanhVien.Add(new ThongKeTangGiamThanhVien() { STT = dem, SLTV = ctm, SLS = ctqhs, SLKH = ctqhvc });   
//                }
                
//                dem = 0;
//                int[] a = new int[LoaiThanhTich.Count];
//                for (int i = InputDateInput.Year; i <= InputDateOutput.Year; i++)
//                {
//                    //dem++;
//                    var cttt = ChiTietThanhTich.Where(q => q.NgayNhap.Year == i).Count();
//                    foreach(ChiTietThanhTich x in ChiTietThanhTich)
//                    {
//                        a[x.IdThanhTich - 1]++;
//                    } 
//                }
//                for(int i=0;i<LoaiThanhTich.Count;i++)
//                {
//                    var ten = LoaiThanhTich.Where(q => q.Id == i + 1).SingleOrDefault().TenThanhTich;
//                    ThongKeThanhTich.Add(new ThongKeThanhTich() { STT = i + 1, LoaiThanhTich = ten, SL = a[i] });
//                }
//            });
//        }
//    }
//    public class ThongKeTangGiamThanhVien
//    {
//        public int STT { get; set; }
//        public int Nam { get; set; }
//        public int SLS { get; set; }
//        public int SLKH { get; set; }
//        public int SLTV { get; set; }
//    }
//    public class ThongKeThanhTich
//    {
//        public int STT { get; set; }
//        public string LoaiThanhTich { get; set; }
//        public int SL { get; set; }
//    }
//}
