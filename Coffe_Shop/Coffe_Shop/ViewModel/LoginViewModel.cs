﻿using Coffe_Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
//using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{

    public class LoginViewModel : BaseViewModel
    {
        public bool IsLogin = false;
        private string _UserName;
        public string UserName { get => _UserName; set { _UserName = value; OnPropertyChanged(); } }
        private string _Pass;
        public string Pass { get => _Pass; set { _Pass = value; OnPropertyChanged(); } }
        private NguoiDung _NguoiDung;
        public NguoiDung NguoiDung { get => _NguoiDung; set { _NguoiDung = value; OnPropertyChanged(); } }
        public ICommand LoginCommand { get; set; }
        public ICommand ExitCommand { get; set; }
        public ICommand PasswordChangedCommand { get; set; }
        public LoginViewModel()
        {
            LoginCommand = new RelayCommand<Window>((p) => { return true; }, (p) => { Login(p); });
            PasswordChangedCommand = new RelayCommand<PasswordBox>((p) => { return true; }, (p) => { Pass = p.Password; });
            ExitCommand = new RelayCommand<Window>((p) => { return true; }, (p) => { p.Close(); });
        }

        void Login(Window p)
        {
            if (p == null)
            {
                return;
            }
            if(UserName != null && Pass != null)
            {
                var x = DataProvider.Ins.DB.DsNguoiDung.Where(q => q.IdNguoiDung == UserName && q.MatKhau == Pass).FirstOrDefault();
                if (x != null)
                {
                    IsLogin = true;
                    NguoiDung = x;
                    p.Close();
                }
                else
                {
                    MessageBox.Show("Sai ten dang nhap hoac mat khau", "Lỗi", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            else if(UserName == "" || Pass == "")
            {
                MessageBox.Show("Bạn phải nhập đầy đủ Tên đăng nhập và mật khẩu", "Lỗi", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show("Sai thông tin đăng nhập", "Lỗi", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
