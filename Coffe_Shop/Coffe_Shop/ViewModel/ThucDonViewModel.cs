﻿using Coffe_Shop.Model;
using Coffe_Shop.Windows;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Coffe_Shop.ViewModel
{
    public class TinhTrang
    {
        public string TenTinhTrang { get; set; }
    }
    
    public class ThucDonViewModel : BaseViewModel
    {
        private string _TenWindow;
        public string TenWindow { get => _TenWindow; set { _TenWindow = value; OnPropertyChanged(); } }
        private string _AnhEdit;
        public string AnhEdit { get => _AnhEdit; set { _AnhEdit = value; OnPropertyChanged(); } }
        private NguoiDung _NguoiDung;
        public NguoiDung NguoiDung { get => _NguoiDung; set { _NguoiDung = value; OnPropertyChanged(); } }

        private ObservableCollection<ThucDon> _DsThucDon;
        public ObservableCollection<ThucDon> DsThucDon { get => _DsThucDon; set { _DsThucDon = value; OnPropertyChanged(); } }
        private ObservableCollection<TinhTrang> _TinhTrang;
        public ObservableCollection<TinhTrang> TinhTrang { get => _TinhTrang; set { _TinhTrang = value; OnPropertyChanged(); } }
        private int _SelectedIndexTinhTrang;
        public int SelectedIndexTinhTrang
        {
            get => _SelectedIndexTinhTrang;
            set
            {
                _SelectedIndexTinhTrang = value;
                OnPropertyChanged();
            }
        }
        private TinhTrang _SelectedTinhTrang;
        public TinhTrang SelectedTinhTrang
        {
            get => _SelectedTinhTrang;
            set
            {
                _SelectedTinhTrang = value;
                OnPropertyChanged();
                if (SelectedTinhTrang != null)
                {
                    ThucDon.TinhTrang = SelectedTinhTrang.TenTinhTrang;
                }
            }
        }
        public ObservableCollection<ChiTietThaoTac> ChiTietThaoTac;
        public List<ChiTietThaoTac> ThaoTacSuaThongTin;
        public List<ChiTietThaoTac> ThaoTacThemMon;
        private ThucDon _SelectedThucDon;
        public ThucDon SelectedThucDon
        {
            get => _SelectedThucDon;
            set
            {
                _SelectedThucDon = value;
                OnPropertyChanged();
            }
        }
        private bool _IsEnabledId;
        public bool IsEnabledId { get => _IsEnabledId; set { _IsEnabledId = value; OnPropertyChanged(); } }
        private bool _IsEnabledTenThucDon;
        public bool IsEnabledTenThucDon { get => _IsEnabledTenThucDon; set { _IsEnabledTenThucDon = value; OnPropertyChanged(); } }
        private ThucDon _ThucDon;
        public ThucDon ThucDon { get => _ThucDon; set { _ThucDon = value; OnPropertyChanged(); } }
        private string _TenThanhVien;
        public string TenThanhVien { get => _TenThanhVien; set { _TenThanhVien = value; OnPropertyChanged(); } }
        public ICommand LoadedWindowCommand { get; set; }
        public ICommand ThemMonCommand { get; set; }
        public ICommand ChonCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand ThemAnhCommand { get; set; }
        public ThucDonWinDow a;
        string selectedFileName;
        bool IsThem = false;
        //bool IsSave = false;
        public ThucDonViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                //DsThucDon = new ObservableCollection<ThucDon>(DataProvider.Ins.DB.DsThucDon);
                TenWindow = "Xem danh sách thực đơn";
                ThucDon = new ThucDon();
                TinhTrang = new ObservableCollection<TinhTrang>();
                TinhTrang.Add(new TinhTrang() { TenTinhTrang = "Còn" });
                TinhTrang.Add(new TinhTrang() { TenTinhTrang = "Hết" });
                SelectedIndexTinhTrang = 0;
                ChiTietThaoTac = new ObservableCollection<ChiTietThaoTac>(DataProvider.Ins.DB.DsChiTietThaoTac.Where(q => q.IdQuyen == NguoiDung.IdQuyen));
                ThaoTacSuaThongTin = ChiTietThaoTac.Where(q => q.IdThaoTac == "Sửa thông tin thực đơn").ToList();
                ThaoTacThemMon = ChiTietThaoTac.Where(q => q.IdThaoTac == "Thêm món mới").ToList();
                if (NguoiDung.IdQuyen == "Admin")
                {
                    IsEnabledId = false;
                    IsEnabledTenThucDon = true;
                }
                else if (ThaoTacSuaThongTin.Count > 0)
                {
                    IsEnabledId = false;
                    IsEnabledTenThucDon = true;
                }
                else
                {
                    IsEnabledId = false;
                    IsEnabledTenThucDon = false;
                }
                selectedFileName = "";
                a = p as ThucDonWinDow;
                AnhEdit = "..\\Image\\Anh00.jpg";
            });
            ThemMonCommand = new RelayCommand<Window>((p) =>
            {
                if (NguoiDung.IdQuyen == "Admin" && IsThem == false)
                    return true;
                else if (ThaoTacThemMon.Count() > 0 && IsThem == false)
                    return true;
                else return false;
            }, (p) =>
            {
                IsEnabledId = true;
                IsEnabledTenThucDon = true;
                ThucDon = new ThucDon
                {
                    TinhTrang = SelectedTinhTrang.TenTinhTrang
                };
                IsThem = true;
                TenWindow = "Thêm món mới";
            });
            ThemAnhCommand = new RelayCommand<Window>((p) =>
            {
                return true;
            }, (p) =>
            {
                //a = p as ThucDonWinDow;
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.InitialDirectory = "f:\\";
                dlg.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
                dlg.RestoreDirectory = true;
                Nullable<bool> dialogOK = dlg.ShowDialog();
                if (dialogOK == true)
                {
                    selectedFileName = dlg.FileName;
                    //FileNameLabel.Content = selectedFileName;
                    AnhEdit = selectedFileName;
                    //a.ImageViewer1.Source = bitmap;
                }
            });
            SaveCommand = new RelayCommand<Window>((p) => 
            {
                var a = p as ThucDonWinDow;
                if (a.tbIdMon.Text != "" && a.tbTenMon.Text != "" && Int32.TryParse(a.tbGiaTien.Text, out int i)) 
                {
                    if (NguoiDung.IdQuyen == "Admin" || ThaoTacSuaThongTin.Count > 0)
                    {
                        if (a.tbTenMon.Text.Length >= 5 && Int32.Parse(a.tbGiaTien.Text) >= 0 && Int32.Parse(a.tbGiaTien.Text) <= 100000)
                            return true;
                        else return false;
                    }
                    if (IsThem)
                    {
                        if (a.tbTenMon.Text.Length >= 5 && Int32.Parse(a.tbGiaTien.Text) >= 0 && Int32.Parse(a.tbGiaTien.Text) <= 100000)
                            return true;
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }, (p) => 
            { 
                if(IsThem)
                {
                    var uri = new Uri(selectedFileName);
                    var bitmap = new BitmapImage(uri);

                    // Save to file.
                    var encoder = new JpegBitmapEncoder(); // Or any other, e.g. PngBitmapEncoder for PNG.
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.QualityLevel = 100; // Set quality level 1-100.
                    var check = false;
                    foreach(var x in DsThucDon)
                    {
                        if(x.Anh == AnhEdit)
                        {
                            check = true;
                            break;
                        }
                    }
                    if(check == false)
                    {
                        
                        var link = @"D:\CoffeShopImage\Anh" + ThucDon.IdThucDon + ".jpg";
                        AnhEdit = link;
                        using (var stream = new FileStream(link, FileMode.Create))
                        {
                            encoder.Save(stream);
                        }
                    }
                    DataProvider.Ins.DB.DsThucDon.Add(new ThucDon() { IdThucDon = ThucDon.IdThucDon, TenThucDon = ThucDon.TenThucDon, GiaTien = ThucDon.GiaTien, TinhTrang = ThucDon.TinhTrang, Anh = AnhEdit });
                    DataProvider.Ins.DB.SaveChanges();
                    DsThucDon.Add(new ThucDon() { IdThucDon = ThucDon.IdThucDon, TenThucDon = ThucDon.TenThucDon, GiaTien = ThucDon.GiaTien, TinhTrang = ThucDon.TinhTrang, Anh = AnhEdit});
                    IsThem = false;
                    IsEnabledTenThucDon = false;
                }
                else
                {
                    var a = DataProvider.Ins.DB.DsThucDon;
                    var uri = new Uri(selectedFileName);
                    var bitmap = new BitmapImage(uri);
                    var encoder = new JpegBitmapEncoder(); // Or any other, e.g. PngBitmapEncoder for PNG.
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.QualityLevel = 100; // Set quality level 1-100.
                    var dem = 0;
                    var check = false;
                    if (check == false)
                    {
                        var link = @"D:\CoffeShopImage\Anh"  + ThucDon.IdThucDon + (ThucDon.LanSua + 1)  + ".jpg";
                        AnhEdit = link;
                        using (var stream = new FileStream(link, FileMode.Create))
                        {
                            encoder.Save(stream);
                        }
                    }
                    foreach (ThucDon x in a)
                    {
                        if(x.IdThucDon == ThucDon.IdThucDon)
                        {
                            x.Anh = AnhEdit;
                            x.TenThucDon = ThucDon.TenThucDon;
                            x.TinhTrang = ThucDon.TinhTrang;
                            x.GiaTien = ThucDon.GiaTien;
                            x.LanSua += 1;
                            DataProvider.Ins.DB.SaveChanges();
                            DsThucDon[dem].TenThucDon = ThucDon.TenThucDon;
                            DsThucDon[dem].TinhTrang = ThucDon.TinhTrang;
                            DsThucDon[dem].GiaTien = ThucDon.GiaTien;
                            DsThucDon[dem].Anh = AnhEdit;
                            break;
                        }
                        dem++;
                    }
                }
                ThucDon = new ThucDon() { TinhTrang = SelectedTinhTrang.TenTinhTrang };
                IsEnabledId = false;
                if (ThaoTacSuaThongTin.Count > 0)
                    IsEnabledTenThucDon = true;
                //IsEnabledTenThucDon = false;
            });
            ChonCommand = new RelayCommand<ThucDon>((p) => 
            { 
                return true;
            }, (p) =>
            {
                if (IsThem)
                {
                    if (MessageBox.Show("Bạn có muốn thoát chức năng thêm mới không?", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if(ThucDon.IdThucDon != "" && ThucDon.TenThucDon !="" && ThucDon.GiaTien != 0 && SelectedTinhTrang != null)
                        {
                            if (MessageBox.Show("Bạn có muốn lưu mới món ăn ko?", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                DataProvider.Ins.DB.DsThucDon.Add(new ThucDon() { IdThucDon = ThucDon.IdThucDon, TenThucDon = ThucDon.TenThucDon, GiaTien = ThucDon.GiaTien, TinhTrang = ThucDon.TinhTrang, Anh = AnhEdit });
                                DataProvider.Ins.DB.SaveChanges();
                                DsThucDon.Add(new ThucDon() { IdThucDon = ThucDon.IdThucDon, TenThucDon = ThucDon.TenThucDon, GiaTien = ThucDon.GiaTien, TinhTrang = ThucDon.TinhTrang, Anh = AnhEdit });
                            }
                        }
                        ThucDon.IdThucDon = p.IdThucDon;
                        ThucDon.TenThucDon = p.TenThucDon;
                        ThucDon.TinhTrang = p.TinhTrang;
                        AnhEdit = p.Anh;
                        ThucDon.LanSua = p.LanSua;
                        for (int i = 0; i < TinhTrang.Count(); i++)
                        {
                            if (TinhTrang[i].TenTinhTrang == p.TinhTrang)
                            {
                                SelectedIndexTinhTrang = i;
                                break;
                            }
                        }
                        ThucDon.GiaTien = p.GiaTien;
                        IsEnabledId = false;
                        IsThem = false;
                        //IsEnabledTenThucDon = true;
                        if (ThaoTacSuaThongTin.Count > 0)
                            IsEnabledTenThucDon = true;
                        TenWindow = "Sửa món " + ThucDon.TenThucDon; 
                    }
                }
                else
                {
                    //IsEnabledTenThucDon = true;
                    ThucDon.IdThucDon = p.IdThucDon;
                    ThucDon.TenThucDon = p.TenThucDon;
                    ThucDon.TinhTrang = p.TinhTrang;
                    ThucDon.LanSua = p.LanSua;

                    AnhEdit = p.Anh;
                    //a.ImageViewer1.Source = bitmap;
                    for (int i = 0; i < TinhTrang.Count(); i++)
                    {
                        if (TinhTrang[i].TenTinhTrang == p.TinhTrang)
                        {
                            SelectedIndexTinhTrang = i;
                            break;
                        }
                    }
                    ThucDon.GiaTien = p.GiaTien;
                    IsEnabledId = false;
                    if(ThaoTacSuaThongTin.Count > 0)
                        IsEnabledTenThucDon = true;
                    IsThem = false;
                    TenWindow = "Sửa món " + ThucDon.TenThucDon;
                }
            });
        }
    }
}
