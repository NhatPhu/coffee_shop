﻿using Coffe_Shop.Model;
using Coffe_Shop.View;
using Coffe_Shop.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class Button : BaseViewModel
    {
        private string _Description;
        public string Description { get => _Description; set { _Description = value; OnPropertyChanged(); } }
        private int _Column;
        public int Column { get => _Column; set { _Column = value; OnPropertyChanged(); } }
        private int _Row;
        public int Row { get => _Row; set { _Row = value; OnPropertyChanged(); } }

    }
    public class MainViewModel : BaseViewModel
    {
        public bool Isloaded = false;

        //private MenuItem _trvMenu;
        //public MenuItem trvMenu { get => _trvMenu; set { _trvMenu = value; OnPropertyChanged(); } }
        public MainWindow mwd;
        
        //private ObservableCollection<ChiTietQuanHe> _ChiTietQuanHe;
        //public ObservableCollection<ChiTietQuanHe> ChiTietQuanHe { get => _ChiTietQuanHe; set { _ChiTietQuanHe = value; OnPropertyChanged(); } }

        //Binding hiển thị danh sách món được chọn
        private ObservableCollection<ViewChonMon> _ViewChonMon;
        public ObservableCollection<ViewChonMon> ViewChonMon { get => _ViewChonMon; set { _ViewChonMon = value; OnPropertyChanged(); } }
        private ViewChonMon _SelectedItemChonMon;
        public ViewChonMon SelectedItemChonMon
        {
            get => _SelectedItemChonMon;
            set
            {
                _SelectedItemChonMon = value;
                OnPropertyChanged();
            }
        }
        private int _SelectedIndexChonMon;
        public int SelectedIndexChonMon { get => _SelectedIndexChonMon; set { _SelectedIndexChonMon = value; OnPropertyChanged(); } }

        //Binding hiển thị danh sách các món có thể chọn.
        private ObservableCollection<ThucDon> _DsThucDon;
        public ObservableCollection<ThucDon> DsThucDon { get => _DsThucDon; set { _DsThucDon = value; OnPropertyChanged(); } }
        private int _SelectedIndexThucDon;
        public int SelectedIndexThucDon { get => _SelectedIndexThucDon; set { _SelectedIndexThucDon = value; OnPropertyChanged(); } }
        
        private ThucDon _SelectedItemThucDon;
        public ThucDon SelectedItemThucDon
        {
            get => _SelectedItemThucDon;
            set
            {
                _SelectedItemThucDon = value;
                OnPropertyChanged();
                //if (SelectedItemThucDon != null && SelectedIndexThucDon >= 0 && SelectedIndexThucDon != null)
                //{
                //    ViewChonMon.Add(new ViewChonMon() { ThucDon = SelectedItemThucDon, SoLuong = 1, TongTien = SelectedItemThucDon.GiaTien });

                //    IsSelected = false;
                //    //SelectedItemThucDon = null;
                   // SelectedIndexThucDon = 1;
                //}
            }
        }

        public NguoiDung NguoiDung = new NguoiDung();
        public int tongTien = 0;
        private string _TextWelcom;
        public string TextWelcom { get => _TextWelcom; set { _TextWelcom = value; OnPropertyChanged(); } }
        private string _TongTien;
        public string TongTien { get => _TongTien; set { _TongTien = value; OnPropertyChanged(); } }
        private DateTime _DateInput;
        public DateTime DateInput { get => _DateInput; set { _DateInput = value; OnPropertyChanged(); } }
        private bool _IsSelector;
        public bool IsSelector { get => _IsSelector; set { _IsSelector = value; OnPropertyChanged(); } }
        private bool _IsSelectedAll; 
        public bool IsSelectedAll
        {
            get => _IsSelectedAll;
            set
            {
                _IsSelectedAll = value;
                OnPropertyChanged();
                if(IsSelectedAll)
                {
                    foreach(ViewChonMon x in ViewChonMon)
                    {
                        x.DaChon = true;
                    }
                }
            }
        }
        private string _TextEditKhachHang;
        public string TextEditKhachHang { get => _TextEditKhachHang; set { _TextEditKhachHang = value; OnPropertyChanged(); } }
        private HinhThucThanhToan _SelectedHinhThuc;
        public HinhThucThanhToan SelectedHinhThuc { get => _SelectedHinhThuc; set { _SelectedHinhThuc = value; OnPropertyChanged(); } }
        private ObservableCollection<HinhThucThanhToan> _HinhThucThanhToan;
        public ObservableCollection<HinhThucThanhToan> HinhThucThanhToan { get => _HinhThucThanhToan; set { _HinhThucThanhToan = value; OnPropertyChanged(); } }
        private KhachHang _SelectedKhachHang;
        public KhachHang SelectedKhachHang { get => _SelectedKhachHang; set { _SelectedKhachHang = value; OnPropertyChanged(); } }
        private ObservableCollection<KhachHang> _KhachHang;
        public ObservableCollection<KhachHang> KhachHang { get => _KhachHang; set { _KhachHang = value; OnPropertyChanged(); } }
        private Thue _SelectedThue;
        public Thue SelectedThue { get => _SelectedThue; set { _SelectedThue = value; OnPropertyChanged(); } }
        private ObservableCollection<Thue> _Thue;
        public ObservableCollection<Thue> Thue { get => _Thue; set { _Thue = value; OnPropertyChanged(); } }

        public ObservableCollection<Button> _Button;
        public ObservableCollection<Button> Button { get => _Button; set { _Button = value; OnPropertyChanged(); } }
        private ObservableCollection<ChiTietThaoTac> _ChiTietThaoTac;
        public ObservableCollection<ChiTietThaoTac> ChiTietThaoTac { get => _ChiTietThaoTac; set { _ChiTietThaoTac = value; OnPropertyChanged(); } }
        private ObservableCollection<ChiTietThaoTac> _ChiTietThaoTac2;
        public ObservableCollection<ChiTietThaoTac> ChiTietThaoTac2 { get => _ChiTietThaoTac2; set { _ChiTietThaoTac2 = value; OnPropertyChanged(); } }

        private string _TimKiem;
        public string TimKiem { get => _TimKiem; set { _TimKiem = value; OnPropertyChanged(); } }
        public ICommand LoadedWindowCommand { get; set; } //Bài Coffe Shop
        public ICommand ChonCommand { get; set; } //Bài Coffy Shop
        public ICommand ChonCommand2 { get; set; } //Bài Coffy Shop
        public ICommand SaveCommand { get; set; }
        public ICommand KhachHangCommand { get; set; }
        public ICommand HoaDonCommand { get; set; }
        public ICommand ThucDonCommand { get; set; }
        public ICommand DatBanCommand { get; set; }
        public ICommand TimKiemCommand { get; set; }
        // mọi thứ xử lý sẽ nằm trong này

        public int Count = 0;
        public MainViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                
                DateInput = DateTime.Now;
                //DateInput = DateTime.Now.Date;
                //TimeSpan a = new TimeSpan();
               
                Isloaded = true;
                LoginWindow loginWindow = new LoginWindow();
                p.Hide();
                var khoiTaoData = DataProvider.Ins.DB.DsThucDon;
                if(!Directory.Exists("D:\\CoffeShopImage"))
                {
                    Directory.CreateDirectory("D:\\CoffeShopImage");
                }
                if (khoiTaoData.Count() <= 0 || khoiTaoData == null)
                {
                    KhoiTao();
                }
                ChiTietThaoTac = new ObservableCollection<ChiTietThaoTac>(DataProvider.Ins.DB.DsChiTietThaoTac);
                DsThucDon = new ObservableCollection<ThucDon>(DataProvider.Ins.DB.DsThucDon);
                Count = DsThucDon.Count;
                ViewChonMon = new ObservableCollection<ViewChonMon>();
                HinhThucThanhToan = new ObservableCollection<HinhThucThanhToan>(DataProvider.Ins.DB.DsHinhThucThanhToan);
                Thue = new ObservableCollection<Thue>(DataProvider.Ins.DB.DsThue);
                KhachHang = new ObservableCollection<KhachHang>(DataProvider.Ins.DB.DsKhachHang);
                SelectedIndexThucDon = -1;
                //SelectedIndexThucDon = 1;
                //IsSelected = false;
                mwd = p as MainWindow;
                //LoadTree();
                loginWindow.ShowDialog();
                var loginVM = loginWindow.DataContext as LoginViewModel;
                if (loginVM.IsLogin)
                {
                    p.Show();
                    this.NguoiDung = loginVM.NguoiDung;
                    TextWelcom = "Chào " + NguoiDung.IdQuyen + " " + NguoiDung.TenNguoiDung;
                }
                else p.Close();
            });
            ThucDonCommand = new RelayCommand<Window>((p) => { return true; }, (p) =>
            {
                ThucDonWinDow thucDonWD = new ThucDonWinDow();
                var thucDonVM = thucDonWD.DataContext as ThucDonViewModel;
                thucDonVM.NguoiDung = NguoiDung;
                thucDonVM.DsThucDon = DsThucDon;
                thucDonWD.ShowDialog();
                //DsThucDon = new ObservableCollection<ThucDon>(DataProvider.Ins.DB.DsThucDon);              
            });

            TimKiemCommand = new RelayCommand<Window>((p) => 
            { 
                if(TimKiem == null || TimKiem == "")
                {
                    if(DsThucDon == null || DsThucDon.Count <= 0 || DsThucDon.Count < Count)
                    {
                        DsThucDon = new ObservableCollection<ThucDon>(DataProvider.Ins.DB.DsThucDon);
                    }
                    return false;
                }
                return true; 
            }, (p) =>
            {
                var thucDon = DataProvider.Ins.DB.DsThucDon.Where(q => q.IdThucDon.ToLower().Contains(TimKiem.ToLower())).ToList();
                DsThucDon = new ObservableCollection<ThucDon>(thucDon);
            });
            DatBanCommand = new RelayCommand<Window>((p) => 
            {
                if (ChiTietThaoTac == null)
                    return false;
                else
                {
                    ChiTietThaoTac2 = new ObservableCollection<ChiTietThaoTac>(ChiTietThaoTac.Where(q => q.IdQuyen == NguoiDung.IdQuyen));
                    var x = ChiTietThaoTac2.Where(q => q.IdThaoTac == "Xem trang đặt bàn");
                    if (NguoiDung.IdQuyen == "Admin")
                        return true;
                    else if (x.Count() > 0)
                        return true;
                    else return false;
                } 
            }, (p) =>
            {
                DatBanWinDow datBanWD = new DatBanWinDow();
                datBanWD.ShowDialog();
                //var thucDonVM = thucDonWD.DataContext as ThucDonViewModel;
                //thucDonVM.NguoiDung = NguoiDung;
                //thucDonVM.DsThucDon = DsThucDon;
                //thucDonWD.ShowDialog();
            });
            ChonCommand = new RelayCommand<string>((p) => { return true; }, (p) =>
            {
                DateInput = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                //DateInput.AddHours(2);
                var x = DataProvider.Ins.DB.DsThucDon.Where(q => q.IdThucDon == p && q.TinhTrang != "Hết").SingleOrDefault();
                var check = false;
                if(x != null)
                {
                    foreach (ViewChonMon i in ViewChonMon)
                    {
                        if (i.ThucDon.IdThucDon == x.IdThucDon)
                        {
                            i.SoLuong++;
                            i.TongTien = i.SoLuong * i.ThucDon.GiaTien;
                            check = true;
                            break;
                        }
                    }
                    if (check == false)
                        ViewChonMon.Add(new ViewChonMon() { ThucDon = x, SoLuong = 1, TongTien = x.GiaTien });
                    IsSelectedAll = false;
                    tongTien += x.GiaTien;
                    TongTien = tongTien + " VND";
                    IsSelector = false;
                }
                
            });
            ChonCommand2 = new RelayCommand<ViewChonMon>((p) => 
            {
                if (SelectedItemChonMon != null)
                    return true;
                else return false;
            }, (p) =>
            {
                //TimKiem tk = new TimKiem();
                //tk.ShowDialog();
                //var a = 1;
                ChiTietHoaDonWinDow hoaDonWD = new ChiTietHoaDonWinDow();
                var hoaDonVM = hoaDonWD.DataContext as ChiTietHoaDonViewModel;
                hoaDonVM.ViewChonMon = p;
                hoaDonWD.ShowDialog();
                tongTien = 0;
                TongTien = "";
                ViewChonMon tam = new ViewChonMon();
                foreach (ViewChonMon x in ViewChonMon)
                {
                    if (x.SoLuong == 0)
                    {
                        tam = x;
                    }
                    else
                        tongTien += x.TongTien;
                }
                TongTien += tongTien + " VND";
                ViewChonMon.Remove(tam);
            });
            SaveCommand = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                //TimKiem tk = new TimKiem();
                //tk.ShowDialog();
                //var a = 1;
                var a = 0;
                if (ViewChonMon.Count > 0 && ViewChonMon != null)
                {
                    foreach (ViewChonMon x in ViewChonMon)
                    {
                        a += x.TongTien;
                    }
                    var i = new HoaDon() {TrangThai = "Còn", IdHoaDon = DateInput, IdHinhThuc = SelectedHinhThuc.IdHinhThuc, IdKhachHang = SelectedKhachHang.IdKhachHang, IdNguoiDung = NguoiDung.IdNguoiDung, TongTien = a, IdThue = SelectedThue.IdThue };
                    DataProvider.Ins.DB.DsHoaDon.Add(i);
                    DataProvider.Ins.DB.SaveChanges();
                    foreach (ViewChonMon x in ViewChonMon)
                    {
                        DataProvider.Ins.DB.DsChiTietHoaDon.Add(new ChiTietHoaDon() { IdHoaDon = i.IdHoaDon, IdThucDon = x.ThucDon.IdThucDon, SoLuong = x.SoLuong, TongTien = x.TongTien, GiaTien = x.ThucDon.GiaTien });
                    }
                    DataProvider.Ins.DB.SaveChanges();
                    ViewChonMon = new ObservableCollection<ViewChonMon>();
                    tongTien = 0;
                    TongTien = "";
                }
                
            });
            HoaDonCommand = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                //TimKiem tk = new TimKiem();
                //tk.ShowDialog();
                //var a = 1;
                HoaDonWindow hoaDonWD = new HoaDonWindow();
               
                hoaDonWD.ShowDialog();

            });
            KhachHangCommand = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                KhachHangWindow khachHangWD = new KhachHangWindow();
                var khachHangVM = khachHangWD.DataContext as KhachHangViewModel;
                khachHangVM.KhachHang = KhachHang;
                khachHangVM.tam2 = TextEditKhachHang;
                khachHangWD.ShowDialog();
                //KhachHang = new ObservableCollection<KhachHang>(DataProvider.Ins.DB.DsKhachHang);
            });
        }


        //private List<ThanhVien>[] _a;
        //public List<ThanhVien>[] a { get => _a; set { _a = value; OnPropertyChanged(); } }

        public void KhoiTao()
        {
            DataProvider.Ins.DB.DsThucDon.Add(new ThucDon() { IdThucDon = "CFS", TenThucDon = "Cà phê sữa", GiaTien = 20000, TinhTrang = "Còn", Anh = "..\\Image\\Anh01.jpg" });
            DataProvider.Ins.DB.DsThucDon.Add(new ThucDon() { IdThucDon = "CFD", TenThucDon = "Cà phê đá", GiaTien = 15000, TinhTrang = "Còn" , Anh = "..\\Image\\Anh02.jpg" });
            DataProvider.Ins.DB.DsQuyen.Add(new Quyen() { IdQuyen = "Admin" });
            DataProvider.Ins.DB.DsQuyen.Add(new Quyen() { IdQuyen = "Thu Ngân" });
            DataProvider.Ins.DB.DsQuyen.Add(new Quyen() { IdQuyen = "Quản lý" });
            DataProvider.Ins.DB.DsQuyen.Add(new Quyen() { IdQuyen = "Đầu bếp" });
            DataProvider.Ins.DB.DsThaoTac.Add(new ThaoTac() { IdThaoTac = "Thêm món mới" });
            DataProvider.Ins.DB.DsThaoTac.Add(new ThaoTac() { IdThaoTac = "Xem trang hóa đơn"});
            DataProvider.Ins.DB.DsThaoTac.Add(new ThaoTac() { IdThaoTac = "Sửa thông tin thực đơn" });
            DataProvider.Ins.DB.DsThaoTac.Add(new ThaoTac() { IdThaoTac = "Phân quyền người dùng" });
            DataProvider.Ins.DB.DsThaoTac.Add(new ThaoTac() { IdThaoTac = "Xem trang đặt bàn" });
            DataProvider.Ins.DB.DsThaoTac.Add(new ThaoTac() { IdThaoTac = "Thêm nhân viên mới" });
            DataProvider.Ins.DB.DsThaoTac.Add(new ThaoTac() { IdThaoTac = "Xóa nhân viên" });
            DataProvider.Ins.DB.DsThaoTac.Add(new ThaoTac() { IdThaoTac = "Chỉnh sửa thông tin thuế, hình thức thanh toán" });
            //DataProvider.Ins.DB.DsChiTietThaoTac.Add(new ChiTietThaoTac() { IdQuyen = "Thu Ngân", IdThaoTac = "Thêm món mới" });
            DataProvider.Ins.DB.DsNguoiDung.Add(new NguoiDung() { IdNguoiDung = "thelaw0825", MatKhau = "1", TenNguoiDung = "Nhật Phú", DiaChi = "Nha Trang", Phone= "0834238897", IdQuyen = "Admin" });
            DataProvider.Ins.DB.DsNguoiDung.Add(new NguoiDung() { IdNguoiDung = "thelaw0826", MatKhau = "1", TenNguoiDung = "Quỳnh Lan", DiaChi = "Nha Trang", Phone = "0834234567", IdQuyen = "Thu Ngân" });
            DataProvider.Ins.DB.DsNguoiDung.Add(new NguoiDung() { IdNguoiDung = "thelaw0827", MatKhau = "1", TenNguoiDung = "Nhật Quý", DiaChi = "Nha Trang", Phone = "0834234567", IdQuyen = "Đầu bếp" });
            DataProvider.Ins.DB.DsHinhThucThanhToan.Add(new HinhThucThanhToan() { IdHinhThuc = "1", TenHinhThuc = "Tiền mặt" });
            DataProvider.Ins.DB.DsKhachHang.Add(new KhachHang() { IdKhachHang = "1", TenKhachHang = "Nhật Trường", DiaChi = "Sài Gòn", SDT = "123456789" });
            DataProvider.Ins.DB.DsThue.Add(new Thue() { IdThue = "1", TenThue = "Thuế VAT", TiGia = 10 });
            DataProvider.Ins.DB.SaveChanges();
        }
    }
} 

