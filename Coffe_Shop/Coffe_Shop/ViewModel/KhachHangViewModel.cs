﻿using Coffe_Shop.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class KhachHangViewModel: BaseViewModel
    {
        private ObservableCollection<KhachHang> _KhachHang;
        public ObservableCollection<KhachHang> KhachHang { get => _KhachHang; set { _KhachHang = value; OnPropertyChanged(); } }
        private KhachHang _SelectedKhachHang;
        public KhachHang SelectedKhachHang { get => _SelectedKhachHang; set { _SelectedKhachHang = value; OnPropertyChanged(); } }
        private string _TenKhachHang;
        public string TenKhachHang { get => _TenKhachHang; set { _TenKhachHang = value; OnPropertyChanged(); } }
        private string _SoDienThoai;
        public string SoDienThoai { get => _SoDienThoai; set { _SoDienThoai = value; OnPropertyChanged(); } }
        private string _DiaChi;
        public string DiaChi { get => _DiaChi; set { _DiaChi = value; OnPropertyChanged(); } }
        public ICommand LoadedWindowCommand { get; set; }
        public ICommand ThemMonCommand { get; set; }
        public ICommand ChonCommand { get; set; }
        public ICommand SaveCommand { get; set; }

        public bool IsThem = false;
        public bool IsSua = false;
        public string IdKhachHang;
        public KhachHang tam;
        public string tam2;
        public KhachHangViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                //KhachHang = new ObservableCollection<KhachHang>(DataProvider.Ins.DB.DsKhachHang);
            });
            ThemMonCommand = new RelayCommand<Window>((p) =>
            {
                if (IsThem == false)
                    return true;
                else return false;
            }, (p) =>
            {
                TenKhachHang = "";
                SoDienThoai = "";
                DiaChi = "";
                IsThem = true;
            });
            SaveCommand = new RelayCommand<Window>((p) =>
            {
                if (IsThem)
                {
                    if (TenKhachHang != "" && SoDienThoai != "" && DiaChi != "")
                    {
                        return true;
                    }
                    else return false;
                }
                else if (IsSua)
                    return true;
                else return false;
            }, (p) =>
            {
                var x = KhachHang.Where(q => q.SDT == SoDienThoai).SingleOrDefault();
                if (x != null && IsThem)
                {
                    MessageBox.Show("Số điện thoại đã tồn tại, nhập số khác", "Thông báo", MessageBoxButton.OK, MessageBoxImage.Error);
                    SoDienThoai = "";
                }
                else
                {
                    var i = KhachHang.Count();
                    if (IsThem)
                    {
                        DataProvider.Ins.DB.DsKhachHang.Add(new KhachHang() { IdKhachHang = (i + 1).ToString(), TenKhachHang = TenKhachHang, SDT = SoDienThoai, DiaChi = DiaChi });
                        DataProvider.Ins.DB.SaveChanges();
                        KhachHang.Add(new KhachHang() { IdKhachHang = (i + 1).ToString(), TenKhachHang = TenKhachHang, SDT = SoDienThoai, DiaChi = DiaChi });
                        IsThem = false;
                        TenKhachHang = "";
                        SoDienThoai = "";
                        DiaChi = "";
                        //IsEnabledTenThucDon = false;
                    }
                    else
                    {
                        var a = DataProvider.Ins.DB.DsKhachHang;
                        var dem = 0;
                        foreach (var k in a)
                        {
                            if (k.IdKhachHang == IdKhachHang)
                            {
                                
                                k.TenKhachHang = TenKhachHang;
                                k.SDT = SoDienThoai;
                                k.DiaChi = DiaChi;
                                DataProvider.Ins.DB.SaveChanges();
                                KhachHang[dem].TenKhachHang = TenKhachHang;
                                KhachHang[dem].SDT = SoDienThoai;
                                KhachHang[dem].DiaChi = DiaChi;
                               // tam2 = KhachHang[dem];
                                break;
                            }
                            dem++;
                        }
                    }
                    //ThucDon = new ThucDon() { TinhTrang = SelectedTinhTrang.TenTinhTrang };
                    IsSua = false;
                }
                //ThucDon.TinhTrang 
                
            });
            ChonCommand = new RelayCommand<KhachHang>((p) =>
            {
                return true;
            }, (p) =>
            {
                IdKhachHang = p.IdKhachHang;
                var i = KhachHang.Count();
                var x = KhachHang.Where(q => q.SDT == SoDienThoai).SingleOrDefault();
                if (IsThem)
                {
                    if (MessageBox.Show("Bạn có muốn thoát chức năng thêm mới không?", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (TenKhachHang != "" && DiaChi != "" && SoDienThoai != "")
                        {
                            if (x == null)
                            {
                                if (MessageBox.Show("Bạn có muốn lưu khách hàng ko ko?", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                {
                                    DataProvider.Ins.DB.DsKhachHang.Add(new KhachHang() { IdKhachHang = i.ToString(), TenKhachHang = TenKhachHang, SDT = SoDienThoai, DiaChi = DiaChi });
                                    DataProvider.Ins.DB.SaveChanges();
                                    KhachHang.Add(new KhachHang() { IdKhachHang = i.ToString(), TenKhachHang = TenKhachHang, SDT = SoDienThoai, DiaChi = DiaChi });
                                    IsThem = false;
                                    //TenKhachHang = "";
                                    //SoDienThoai = "";
                                    //DiaChi = "";
                                }

                            }
                            else
                            {
                                MessageBox.Show("Số điện thoại đã tồn tại, nhập số khác", "Thông báo", MessageBoxButton.OK, MessageBoxImage.Error);
                                SoDienThoai = "";
                                if (MessageBox.Show("Bạn có muốn ở lại chức năng thêm để sửa số điện thoại ko", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                {
                                    return;
                                }
                            }
                        }
                    }
                    else return;
                }
                SoDienThoai = p.SDT;
                TenKhachHang = p.TenKhachHang;
                DiaChi = p.DiaChi;
                IsSua = true;
                IsThem = false;
                tam = p;
            });
        }
    }
}
