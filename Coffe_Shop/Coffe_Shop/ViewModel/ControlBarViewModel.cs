﻿using Coffe_Shop.Model;
using Coffe_Shop.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class ControlBarViewModel : BaseViewModel
    {
        #region commands
        public ICommand CloseWindowCommand { get; set; }
        public ICommand MaximizeWindowCommand { get; set; }
        public ICommand MinimizeWindowCommand { get; set; }
        public ICommand MouseMoveWindowCommand { get; set; }
        public ICommand LoadedWindowCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand DanhMucCommand { get; set; }
        public ICommand DoiPassCommand { get; set; }
        public ICommand LogOutCommand { get; set; }
        public ICommand ThemNVCommand { get; set; }
      
        
        #endregion
        private Boolean _Check { get; set; }
        public Boolean Check { get => _Check; set { _Check = value; OnPropertyChanged(); } }
        private int _WidthBackButton { get; set; }
        public int WidthBackButton { get => _WidthBackButton; set { _WidthBackButton = value; OnPropertyChanged(); } }
        public DateTime date;
        private Boolean _IsEnablePhanQuyen { get; set; }
        public Boolean IsEnablePhanQuyen { get => _IsEnablePhanQuyen; set { _IsEnablePhanQuyen = value; OnPropertyChanged(); } }
        private Boolean _IsEnableThemNV { get; set; }
        public Boolean IsEnableThemNV { get => _IsEnableThemNV; set { _IsEnableThemNV = value; OnPropertyChanged(); } }
        private Boolean _IsEnableDanhMuc { get; set; }
        public Boolean IsEnableDanhMuc { get => _IsEnableDanhMuc; set { _IsEnableDanhMuc = value; OnPropertyChanged(); } }
        public int tam;
        public ControlBarViewModel()
        {
            CloseWindowCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) => {
                FrameworkElement window = GetWindowParent(p);
                if (window is Window w)
                {
                    w.Close();
                }
            });
            MaximizeWindowCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) =>
            {
                FrameworkElement window = GetWindowParent(p);
                if (window is Window w)
                {
                    if (w.WindowState != WindowState.Maximized)
                        w.WindowState = WindowState.Maximized;
                    else
                        w.WindowState = WindowState.Normal;
                }
            });
            MinimizeWindowCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) =>
            {
                FrameworkElement window = GetWindowParent(p);
                if (window is Window w)
                {
                    if (w.WindowState != WindowState.Minimized)
                        w.WindowState = WindowState.Minimized;
                    else
                        w.WindowState = WindowState.Maximized;
                }
            });
            MouseMoveWindowCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) =>
            {
                FrameworkElement window = GetWindowParent(p);
                if (window is Window w)
                {
                    w.DragMove();
                }
            });
            LoadedWindowCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) => {
                FrameworkElement window = GetWindowParent(p);
                var w = window as Window;
                var q = window as LoginWindow;
                var tv = window as ThanhVienWindow;
                var main = window as MainWindow;
                var cthd = window as ChiTietHoaDonWinDow;
                var hd = window as HoaDonWindow;
                var nv = window as NhanVienWindow;
                var kh = window as KhachHangWindow;
                var pq = window as PhanQuyenWindow;
                Check = false;
                WidthBackButton = 0;
                if (w != null && w == main)
                {
                    Check = true;
                }
                else Check = false;
                if (w != null && w == main)
                {
                    var mainVM = w.DataContext as MainViewModel;
                    var chiTietThaoTac = DataProvider.Ins.DB.DsChiTietThaoTac.Where(k => k.IdQuyen == mainVM.NguoiDung.IdQuyen);
                    var thaoTacPhanQuyen = chiTietThaoTac.Where(k => k.IdThaoTac == "Phân quyền người dùng");
                    var thaoTacThemNV = chiTietThaoTac.Where(k => k.IdThaoTac == "Thêm nhân viên mới");
                    var thaoTacXoaNV = chiTietThaoTac.Where(k => k.IdThaoTac == "Xóa nhân viên");
                    var thaoTacDanhMuc = chiTietThaoTac.Where(k => k.IdThaoTac == "Chỉnh sửa thông tin thuế, hình thức thanh toán");
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacPhanQuyen.Count() > 0)
                    {
                        IsEnablePhanQuyen = true;
                    }
                    else
                    {
                        IsEnablePhanQuyen = false;
                    }
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacThemNV.Count() > 0)
                    {
                      
                        IsEnableThemNV = true;
                    }
                    else
                    {
                        IsEnableThemNV = false;
                    }
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacXoaNV.Count() > 0)
                    {
                        WidthBackButton = 50;
                    }
                    else
                    {
                        WidthBackButton = 0;
                    }
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacDanhMuc.Count() > 0)
                    {

                        IsEnableDanhMuc = true;
                    }
                    else
                    {
                        IsEnableDanhMuc = false;
                    }
                }
                
            });
            AddCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) => {
                FrameworkElement window = GetWindowParent(p);
                PhanQuyenWindow phanQuyenWD = new PhanQuyenWindow();
                //ThanhVienViewModel tvVM = tv.DataContext as ThanhVienViewModel;
                //tvVM.isAdd = true;
                var phanQuyenVM = phanQuyenWD.DataContext as PhanQuyenViewModel;
                var main = window as MainWindow;
                phanQuyenWD.ShowDialog();
                if (window is Window w && main == w)
                {
                    var mainVM = w.DataContext as MainViewModel;
                    mainVM.ChiTietThaoTac = new ObservableCollection<ChiTietThaoTac>(DataProvider.Ins.DB.DsChiTietThaoTac);
                    var chiTietThaoTac = mainVM.ChiTietThaoTac.Where(k => k.IdQuyen == mainVM.NguoiDung.IdQuyen);
                    var thaoTacPhanQuyen = chiTietThaoTac.Where(k => k.IdThaoTac == "Phân quyền người dùng");
                    var thaoTacThemNV = chiTietThaoTac.Where(k => k.IdThaoTac == "Thêm nhân viên mới");
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacPhanQuyen.Count() > 0)
                    {
                        IsEnablePhanQuyen = true;
                        //IsEnableThemNV = true;
                    }
                    else
                    {
                        IsEnablePhanQuyen = false;
                    }
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacThemNV.Count() > 0)
                    {
                        IsEnableThemNV = true;
                    }
                    else
                    {
                        IsEnableThemNV = false;
                    }
                }
            });
            LogOutCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) => {
                tam = 1;
                FrameworkElement window = GetWindowParent(p);
                var main = window as MainWindow;
                if (window is Window w && main == w)
                {
                    ReLoad(main);
                }

                //var mw = window as MainWindow;
                //if (window == mw)
                //{
                //    mw.trvMenu.Items.Clear();
                //    var mainVM = mw.DataContext as MainViewModel;
                //    mainVM.LoadTree();
                //}
            });
            ThemNVCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) => {
                FrameworkElement window = GetWindowParent(p);
                var w = window as Window;
                var main = window as MainWindow;
                NhanVienWindow nhanVienWD = new NhanVienWindow();
                var nhanVienVM = nhanVienWD.DataContext as NhanVienViewModel;
                nhanVienVM.WidthBackButton = WidthBackButton;
                nhanVienWD.ShowDialog();
            });
            DanhMucCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) => {
                FrameworkElement window = GetWindowParent(p);
                var w = window as Window;
                DanhMuc danhMucWD = new DanhMuc();
                danhMucWD.ShowDialog();
            });
            DoiPassCommand = new RelayCommand<UserControl>((p) => { return p == null ? false : true; }, (p) => {
                FrameworkElement window = GetWindowParent(p);
                SuaThongTinWindow suaThongTinWD = new SuaThongTinWindow();
                var suaThongTinVM = suaThongTinWD.DataContext as SuaThongTinViewModel;
                //suaThongTinVM
                var mainWD = window as MainWindow;
                if (window is Window w && w == mainWD)
                {
                    var mainVM = w.DataContext as MainViewModel;
                    suaThongTinVM.NguoiDung = mainVM.NguoiDung;
                }
                suaThongTinWD.ShowDialog();
            });
        }
        FrameworkElement GetWindowParent(UserControl p)
        {
            FrameworkElement parent = p;
            
            while (parent.Parent != null)
            {
                parent = parent.Parent as FrameworkElement;
            }
            return parent;
        }
        void ReLoad(MainWindow main)
        {
            var mainVM = main.DataContext as MainViewModel;
            main.Hide();
            var check = false;
            while (check == false)
            {
                LoginWindow loginWD = new LoginWindow();
                var loginVM = loginWD.DataContext as LoginViewModel;
                loginVM.IsLogin = false;
                loginWD.ShowDialog();
                if (loginVM.IsLogin)
                {
                    check = true;
                    main.Show();
                    //var mainVM = main.DataContext as MainViewModel;
                    mainVM.NguoiDung = loginVM.NguoiDung;
                    mainVM.TextWelcom = "Chào " + mainVM.NguoiDung.IdQuyen + " " + mainVM.NguoiDung.TenNguoiDung;
                    //var mainVM = w.DataContext as MainViewModel;
                    var chiTietThaoTac = DataProvider.Ins.DB.DsChiTietThaoTac.Where(k => k.IdQuyen == mainVM.NguoiDung.IdQuyen);
                    var thaoTacPhanQuyen = chiTietThaoTac.Where(k => k.IdThaoTac == "Phân quyền người dùng");
                    var thaoTacThemNV = chiTietThaoTac.Where(k => k.IdThaoTac == "Thêm nhân viên mới");
                    var thaoTacXoaNV = chiTietThaoTac.Where(k => k.IdThaoTac == "Xóa nhân viên");
                    var thaoTacDanhMuc = chiTietThaoTac.Where(k => k.IdThaoTac == "Chỉnh sửa thông tin thuế, hình thức thanh toán");
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacPhanQuyen.Count() > 0)
                    {
                        IsEnablePhanQuyen = true;
                        //IsEnableThemNV = true;
                    }
                    else
                    {
                        IsEnablePhanQuyen = false;
                    }
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacThemNV.Count() > 0)
                    {
                        IsEnableThemNV = true;
                    }
                    else
                    {
                        IsEnableThemNV = false;
                    }
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacXoaNV.Count() > 0)
                    {
                        WidthBackButton = 50;

                    }
                    else
                    {
                        WidthBackButton = 0;
                    }
                    if (mainVM.NguoiDung.IdQuyen == "Admin" || thaoTacDanhMuc.Count() > 0)
                    {

                        IsEnableDanhMuc = true;
                    }
                    else
                    {
                        IsEnableDanhMuc = false;
                    }
                }
                else
                {
                    if (MessageBox.Show("Bạn có muốn thoát khỏi chương trình không?", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        check = true;
                        main.Close();
                    }
                }
            }
        }
    }
}
