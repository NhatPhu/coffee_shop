﻿using Coffe_Shop.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class ChiTietHoaDonViewModel : BaseViewModel
    {
        private ViewChonMon _ViewChonMon;
        public ViewChonMon ViewChonMon { get => _ViewChonMon; set { _ViewChonMon = value; OnPropertyChanged(); } }

        private string _TenMon;
        public string TenMon { get => _TenMon; set { _TenMon = value; OnPropertyChanged(); } }
        public bool IsSave = false;
        public ICommand LoadedWindowCommand { get; set; }
        public ICommand CongCommand { get; set; }
        public ICommand TruCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ChiTietHoaDonViewModel()
        {

            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                if(ViewChonMon != null)
                {
                    TenMon = ViewChonMon.ThucDon.TenThucDon;
                }
            });
            CongCommand = new RelayCommand<Window>((p) => { return true; }, (p) => 
            {
                ViewChonMon.SoLuong++;
                ViewChonMon.TongTien = ViewChonMon.SoLuong * ViewChonMon.ThucDon.GiaTien;
            });
            TruCommand = new RelayCommand<Window>((p) =>
            {
                if (ViewChonMon.SoLuong > 0)
                    return true;
                else return false;
            }, (p) =>
            {
                ViewChonMon.SoLuong--;
                ViewChonMon.TongTien = ViewChonMon.SoLuong * ViewChonMon.ThucDon.GiaTien;
            });

            SaveCommand = new RelayCommand<Window>((p) => 
            {
                return true;
            }, (p) => 
            {
                IsSave = true;
                p.Close();
            });
        }
    }
}
