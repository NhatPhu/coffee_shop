﻿using Coffe_Shop.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class NhanVienViewModel : BaseViewModel
    {
        private int _WidthBackButton { get; set; }
        public int WidthBackButton { get => _WidthBackButton; set { _WidthBackButton = value; OnPropertyChanged(); } }
        private ObservableCollection<Quyen> _Quyen;
        public ObservableCollection<Quyen> Quyen { get => _Quyen; set { _Quyen = value; OnPropertyChanged(); } }
        private Quyen _SelectedQuyen;
        public Quyen SelectedQuyen { get => _SelectedQuyen; set { _SelectedQuyen = value; OnPropertyChanged(); } }
        private ObservableCollection<NguoiDung> _NguoiDung;
        public ObservableCollection<NguoiDung> NguoiDung { get => _NguoiDung; set { _NguoiDung = value; OnPropertyChanged(); } }
        private NguoiDung _SelectedNguoiDung;
        public NguoiDung SelectedNguoiDung { get => _SelectedNguoiDung; set { _SelectedNguoiDung = value; OnPropertyChanged(); } }
        private string _TenDangNhap;
        public string TenDangNhap { get => _TenDangNhap; set { _TenDangNhap = value; OnPropertyChanged(); } }
        private string _TenNguoiDung;
        public string TenNguoiDung { get => _TenNguoiDung; set { _TenNguoiDung = value; OnPropertyChanged(); } }
        private string _SDT;
        public string SDT { get => _SDT; set { _SDT = value; OnPropertyChanged(); } }
        private Boolean _IsEnabledId;
        public Boolean IsEnabledId { get => _IsEnabledId; set { _IsEnabledId = value; OnPropertyChanged(); } }
        public ICommand LoadedWindowCommand { get; set; } //Bài Coffe Shop
        public ICommand ChonCommand { get; set; } //Bài Coffy Shop
        public ICommand SaveCommand { get; set; }
        public ICommand ThemMonCommand { get; set; }
        public ICommand ResetCommand { get; set; }

        public Boolean IsThem = false;
        public Boolean IsSua = false;
        public NhanVienViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                Quyen = new ObservableCollection<Quyen>(DataProvider.Ins.DB.DsQuyen.Where(q => q.IdQuyen != "Quản lý"));
                NguoiDung = new ObservableCollection<NguoiDung>(DataProvider.Ins.DB.DsNguoiDung.Where(q => q.IdQuyen != "Quản lý"));
                IsEnabledId = false;
            });
            ThemMonCommand = new RelayCommand<Window>((p) =>
            {
                if (IsThem == false)
                    return true;
                else return false;
            }, (p) =>
            {
                TenDangNhap = "";
                SDT = "";
                TenNguoiDung = "";
                IsEnabledId = true;
                IsThem = true;
            });
            SaveCommand = new RelayCommand<Window>((p) =>
            {
                if (IsThem)
                {
                    if (TenDangNhap != "" && TenNguoiDung != "" && SDT != "")
                    {
                        return true;
                    }
                    else return false;
                }
                else if (IsSua)
                    return true;
                else return false;
            }, (p) =>
            {
                var x = NguoiDung.Where(q => q.IdNguoiDung == TenDangNhap).SingleOrDefault();
                if (x != null)
                {
                    MessageBox.Show("Tên đăng nhập đã tồn tại", "Thông báo", MessageBoxButton.OK, MessageBoxImage.Error);
                    TenDangNhap = "";
                }
                else
                {
                    //var i = KhachHang.Count();
                    if (IsThem)
                    {
                        DataProvider.Ins.DB.DsNguoiDung.Add(new NguoiDung() { MatKhau = "1", IdNguoiDung = TenDangNhap, TenNguoiDung = TenNguoiDung, Phone = SDT, IdQuyen = SelectedQuyen.IdQuyen});
                        DataProvider.Ins.DB.SaveChanges();
                        NguoiDung.Add(new NguoiDung() { MatKhau = "1", IdNguoiDung = TenDangNhap, TenNguoiDung = TenNguoiDung, Phone = SDT, IdQuyen = SelectedQuyen.IdQuyen });
                        IsThem = false;
                        IsEnabledId = false;
                        TenNguoiDung = "";
                        TenDangNhap = "";
                        SDT = "";
                        //IsEnabledTenThucDon = false;
                    }
                    else
                    {
                        var a = DataProvider.Ins.DB.DsNguoiDung;
                        var dem = 0;
                        foreach (var k in a)
                        {
                            if (k.IdNguoiDung == TenDangNhap)
                            {
                                k.IdNguoiDung = TenDangNhap;
                                k.Phone = SDT;
                                k.TenNguoiDung = TenNguoiDung;
                                DataProvider.Ins.DB.SaveChanges();
                                NguoiDung[dem].TenNguoiDung = TenNguoiDung;
                                NguoiDung[dem].Phone = SDT;
                                NguoiDung[dem].IdNguoiDung = TenDangNhap;
                                break;
                            }
                            dem++;
                        }
                    }
                    //ThucDon = new ThucDon() { TinhTrang = SelectedTinhTrang.TenTinhTrang };
                    IsSua = false;
                }
            });
            ChonCommand = new RelayCommand<NguoiDung>((p) =>
            {
                return true;
            }, (p) =>
            {
                //TenDangNhap = p.IdNguoiDung;
               // var i = KhachHang.Count();
                var x = NguoiDung.Where(q => q.IdNguoiDung == TenDangNhap).SingleOrDefault();
                if (IsThem)
                {
                    if (MessageBox.Show("Bạn có muốn thoát chức năng thêm mới không?", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (TenDangNhap != "" && SDT != "" && TenNguoiDung != "")
                        {
                            if (x == null)
                            {
                                if (MessageBox.Show("Bạn có muốn lưu lại nhân viên này ko?", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                {
                                    DataProvider.Ins.DB.DsNguoiDung.Add(new NguoiDung() { MatKhau = "1", IdNguoiDung = TenDangNhap, TenNguoiDung = TenNguoiDung, Phone = SDT, IdQuyen = SelectedQuyen.IdQuyen});
                                    DataProvider.Ins.DB.SaveChanges();
                                    NguoiDung.Add(new NguoiDung() { MatKhau = "1", IdNguoiDung = TenDangNhap, TenNguoiDung = TenNguoiDung, Phone = SDT, IdQuyen = SelectedQuyen.IdQuyen });
                                    IsThem = false;
                                    IsEnabledId = false;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Tên đăng nhập đã tồn tại, nhập tên khác", "Thông báo", MessageBoxButton.OK, MessageBoxImage.Error);
                                TenDangNhap = "";
                                if (MessageBox.Show("Bạn có muốn ở lại chức năng thêm để sửa tên đăng nhập ko", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                {
                                    return;
                                }
                            }
                        }
                    }
                    else return;
                }
                for(int i =0;i<Quyen.Count();i++)
                {
                    if(Quyen[i].IdQuyen == p.IdQuyen)
                    {
                        SelectedQuyen = Quyen[i];
                        break;
                    }
                }
                //SelectedQuyen = 
                SDT = p.Phone;
                TenDangNhap = p.IdNguoiDung;
                TenNguoiDung = p.TenNguoiDung;
                IsSua = true;
                IsThem = false;
            });
            ResetCommand = new RelayCommand<Window>((p) =>
            {
                if (SelectedNguoiDung != null)
                    return true;
                else return false;
            }, (p) => 
            {
                var x = DataProvider.Ins.DB.DsNguoiDung.Where(q => q.IdNguoiDung == SelectedNguoiDung.IdNguoiDung).SingleOrDefault();
                x.MatKhau = "1";
                DataProvider.Ins.DB.SaveChanges();
            });
        }
    }
}
