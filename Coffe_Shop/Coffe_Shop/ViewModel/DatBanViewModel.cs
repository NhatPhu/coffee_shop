﻿using Coffe_Shop.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class TinhTrangDatBan
    {
        public string TenTinhTrang { get; set; }
    }
    public class TinhTrangBan
    {
        public string TenTinhTrang { get; set; }
    }
    public class DatBanViewModel : BaseViewModel
    {
        private int _SoLuongBan;
        public int SoLuongBan { get => _SoLuongBan; set { _SoLuongBan = value; OnPropertyChanged(); } }
        private ObservableCollection<TinhTrangDatBan> _TinhTrang;
        public ObservableCollection<TinhTrangDatBan> TinhTrang { get => _TinhTrang; set { _TinhTrang = value; OnPropertyChanged(); } }
        private TinhTrangDatBan _SelectedTinhTrang;
        public TinhTrangDatBan SelectedTinhTrang
        {
            get => _SelectedTinhTrang;
            set
            {
                _SelectedTinhTrang = value;
                OnPropertyChanged();
                
            }
        }
        private int _SelectedIdnexTinhTrang;
        public int SelectedIdnexTinhTrang { get => _SelectedIdnexTinhTrang; set { _SelectedIdnexTinhTrang = value; OnPropertyChanged(); } }
        
        private ObservableCollection<TinhTrangBan> _TinhTrangBan;
        public ObservableCollection<TinhTrangBan> TinhTrangBan { get => _TinhTrangBan; set { _TinhTrangBan = value; OnPropertyChanged(); } }
        private TinhTrangBan _SelectedTinhTrangBan;
        public TinhTrangBan SelectedTinhTrangBan { get => _SelectedTinhTrangBan; set { _SelectedTinhTrangBan = value; OnPropertyChanged(); } }
        private ObservableCollection<KhachHang> _KhachHang;
        public ObservableCollection<KhachHang> KhachHang { get => _KhachHang; set { _KhachHang = value; OnPropertyChanged(); } }
        private KhachHang _SelectedKhachHang;
        public KhachHang SelectedKhachHang { get => _SelectedKhachHang; set { _SelectedKhachHang = value; OnPropertyChanged(); } }
        private ObservableCollection<Ban> _Ban;
        public ObservableCollection<Ban> Ban { get => _Ban; set { _Ban = value; OnPropertyChanged(); } }
        private Ban _SelectedBan;
        public Ban SelectedBan { get => _SelectedBan; set { _SelectedBan = value; OnPropertyChanged(); } }
        private ObservableCollection<ChiTietDatBan> _ChiTietDatBanHomNay;
        public ObservableCollection<ChiTietDatBan> ChiTietDatBanHomNay { get => _ChiTietDatBanHomNay; set { _ChiTietDatBanHomNay = value; OnPropertyChanged(); } }
        private ChiTietDatBan _SelectedChiTietDatBanHomNay;
        public ChiTietDatBan SelectedChiTietDatBanHomNay
        {
            get => _SelectedChiTietDatBanHomNay;
            set
            {
                _SelectedChiTietDatBanHomNay = value;
                OnPropertyChanged();
                if (SelectedChiTietDatBanHomNay != null)
                {
                    if (SelectedChiTietDatBanHomNay.TrangThai == "Chờ")
                        SelectedTinhTrang = TinhTrang[0];
                }
            }
        }
        private ChiTietDatBan _SelectedChiTietDatBanDcChon;
        public ChiTietDatBan SelectedChiTietDatBanDcChon { get => _SelectedChiTietDatBanDcChon; set { _SelectedChiTietDatBanDcChon = value; OnPropertyChanged(); } }
        private ObservableCollection<ChiTietDatBan> _ChiTietDatBanDcChon;
        public ObservableCollection<ChiTietDatBan> ChiTietDatBanDcChon { get => _ChiTietDatBanDcChon; set { _ChiTietDatBanDcChon = value; OnPropertyChanged(); } }
        private ObservableCollection<ChiTietDatBan> _ChiTietDatBan;
        public ObservableCollection<ChiTietDatBan> ChiTietDatBan { get => _ChiTietDatBan; set { _ChiTietDatBan = value; OnPropertyChanged(); } }
       
        private string _TenBan;
        public string TenBan { get => _TenBan; set { _TenBan = value; OnPropertyChanged(); } }
        private int _SoBan;
        public int SoBan { get => _SoBan; set { _SoBan = value; OnPropertyChanged(); } }
        private DateTime _NgayDat;
        public DateTime NgayDat { get => _NgayDat; set { _NgayDat = value; OnPropertyChanged(); } }
        private DateTime _ThoiGian;
        public DateTime ThoiGian { get => _ThoiGian; set { _ThoiGian = value; OnPropertyChanged(); } }
        public ICommand LoadedWindowCommand { get; set; } //Bài Coffe Shop
        public ICommand ChonCommand { get; set; } //Bài Coffy Shop
        public ICommand ChonCommand2 { get; set; } //Bài Coffy Shop
        public ICommand SaveCommand { get; set; }
        public ICommand ThemBanCommand { get; set; }
        public ICommand ThemDatBanCommand { get; set; }
        public ICommand BanCommand { get; set; }
        public ICommand HuyDatCommand { get; set; }
        public ICommand LuuDatCommand { get; set; }
        public string luuVet;
        private string _BanDcChon;
        public string BanDcChon { get => _BanDcChon; set { _BanDcChon = value; OnPropertyChanged(); } }
        //private bool _IsSelectedAll;
        //public bool IsSelectedAll
        //{
        //    get => _IsSelectedAll;
        //    set
        //    {
        //        _IsSelectedAll = value;
        //        OnPropertyChanged();
        //        if (IsSelectedAll)
        //        {
        //            foreach (ChiTietDatBan x in ChiTietDatBanHomNay)
        //            {
        //                x.TrangThai = true;
        //            }
        //        }
        //    }
        //}
        //private bool _IsSelectedAll2;
        //public bool IsSelectedAll2
        //{
        //    get => _IsSelectedAll2;
        //    set
        //    {
        //        _IsSelectedAll2 = value;
        //        OnPropertyChanged();
        //        if (IsSelectedAll)
        //        {
        //            foreach (ChiTietDatBan x in ChiTietDatBanDcChon)
        //            {
        //                x.TrangThai = true;
        //            }
        //        }
        //    }
        //}
        public DatBanViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                Ban = new ObservableCollection<Ban>(DataProvider.Ins.DB.DsBan);
                TenBan = "Bàn ";
                TinhTrang = new ObservableCollection<TinhTrangDatBan>();
                TinhTrang.Add(new TinhTrangDatBan() { TenTinhTrang = "Chờ" });
                TinhTrang.Add(new TinhTrangDatBan() { TenTinhTrang = "Hủy" });
                TinhTrang.Add(new TinhTrangDatBan() { TenTinhTrang = "Hoàn thành" });
                TinhTrangBan = new ObservableCollection<TinhTrangBan>();
                TinhTrangBan.Add(new TinhTrangBan() { TenTinhTrang = "Còn trống" });
                TinhTrangBan.Add(new TinhTrangBan() { TenTinhTrang = "Có khách" });
                ChiTietDatBan = new ObservableCollection<ChiTietDatBan>(DataProvider.Ins.DB.DsChiTietDatBan.Where(q=>q.TrangThai == "Chờ"));
                ChiTietDatBanHomNay = new ObservableCollection<ChiTietDatBan>(ChiTietDatBan.Where(q=>q.NgayDat > DateTime.Now && q.NgayDat.Date == DateTime.Now.Date));
                KhachHang = new ObservableCollection<KhachHang>(DataProvider.Ins.DB.DsKhachHang);
                NgayDat = DateTime.Now.Date;
                ThoiGian = DateTime.Now;
                BanDcChon = "Chọn 1 bàn để xem lịch đặt bàn";
                var x = ChiTietDatBan.Where(q => q.TrangThai == "Chờ" && DateTime.Now.TimeOfDay.Hours - q.NgayDat.TimeOfDay.Hours >= 2 && DateTime.Now.Date == NgayDat.Date);
                foreach (var i in x)
                {
                    i.TrangThai = "Hủy";
                    var k = DataProvider.Ins.DB.DsChiTietDatBan.Where(q => q.IdKhachHang == i.IdKhachHang && q.NgayDat == i.NgayDat).SingleOrDefault();
                    k.TrangThai = "Hủy";
                }
                //ChiTietDatBanDcChon = new ObservableCollection<ChiTietDatBan>(ChiTietDatBan.Where(q => q.IdBan == Ban[0].IdBan));
            });
            ThemBanCommand = new RelayCommand<object>((p) => 
            {
                if (SoBan == 0)
                    return false;
                else return true;
            }, (p) => 
            {
                var x = Ban.Where(q => q.IdBan == TenBan + SoBan);
                if(x.Count() > 0)
                {
                    MessageBox.Show("Bàn đã tồn tại, nhập tên khác", "Cảnh báo", MessageBoxButton.OK, MessageBoxImage.Error);
                    SoBan = 0;
                }
                else
                {
                    Ban.Add(new Ban() { IdBan = TenBan + SoBan, TinhTrang = false, Mau = "Blue" });
                    DataProvider.Ins.DB.DsBan.Add(new Ban() { IdBan = TenBan + SoBan, TinhTrang = false, Mau = "Blue" });
                    DataProvider.Ins.DB.SaveChanges();
                    SoBan = 0;
                }
            });
            ThemDatBanCommand = new RelayCommand<object>((p) =>
            {
                //var x = new TimeSpan(2, 0, 0);
                if (NgayDat.Date < DateTime.Now.Date && SoLuongBan != 0)
                    return false;
                else if (NgayDat.Date == DateTime.Now.Date)
                {
                    if (ThoiGian.TimeOfDay.Hours - DateTime.Now.TimeOfDay.Hours >= 2)
                    {
                        return true;
                    }
                    else return false;
                }
                else return true;
            }, (p) =>
            {
                var kh = ChiTietDatBan.Where(q => q.IdKhachHang == SelectedKhachHang.IdKhachHang && q.NgayDat.Date == DateTime.Now.Date && q.TrangThai == "Chờ").SingleOrDefault();
                if(kh != null)
                {
                    MessageBox.Show("Khách hàng này đã đặt bàn hôm nay nhưng chưa đến", "Thông báo", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    var x = new ChiTietDatBan() { IdKhachHang = SelectedKhachHang.IdKhachHang, NgayDat = NgayDat.Date + ThoiGian.TimeOfDay, SoLuong = SoLuongBan };
                    var i = DataProvider.Ins.DB.DsChiTietDatBan.Where(q => q.IdKhachHang == x.IdKhachHang && q.NgayDat == x.NgayDat).SingleOrDefault();
                    if (i != null)
                    {
                        if (i.TrangThai == "Hủy")
                            i.TrangThai = "Chờ";
                    }
                    else
                    {
                        x.TrangThai = "Chờ";
                        DataProvider.Ins.DB.DsChiTietDatBan.Add(x);
                    }

                    ChiTietDatBan.Add(x);

                    if (NgayDat.Date == DateTime.Now.Date)
                    {
                        ChiTietDatBanHomNay.Add(x);
                    }
                    DataProvider.Ins.DB.SaveChanges();
                }
                //TimeSpan time = new TimeSpan(ThoiGian.TimeOfDay.Hours, ThoiGian.TimeOfDay.Minutes, )
                
            });
            LuuDatCommand = new RelayCommand<object>((p) =>
            {
                //if()
                return true;
            }, (p) =>
            {
                var x = DataProvider.Ins.DB.DsBan.Where(q => q.IdBan == luuVet).SingleOrDefault();
                var y = Ban.Where(q => q.IdBan == luuVet).SingleOrDefault();
                if(x!=null)
                {
                    if (SelectedTinhTrangBan.TenTinhTrang == "Còn trống")
                    {
                        x.Mau = "Blue";
                        x.TinhTrang = false;
                        y.Mau = "Blue";
                        y.TinhTrang = false;
                    }
                    else
                    {
                        x.Mau = "Red";
                        x.TinhTrang = true;
                        y.Mau = "Red";
                        y.TinhTrang = true;
                    }
                    DataProvider.Ins.DB.SaveChanges();
                }
                
            });
            BanCommand = new RelayCommand<string>((p) => { return true; }, (p) =>
               {
                   BanDcChon = "Tình trạng " + p;
                   //ChiTietDatBanDcChon = new ObservableCollection<ChiTietDatBan>(ChiTietDatBan.Where(q => q.IdBan == p));
                   luuVet = p;
                   var x = Ban.Where(q => q.IdBan == p).SingleOrDefault();
                   if(x.TinhTrang)
                   {
                       SelectedTinhTrangBan = TinhTrangBan[1];
                   }
                   else
                       SelectedTinhTrangBan = TinhTrangBan[0];
               });
            HuyDatCommand = new RelayCommand<string>((p) => 
            {
                if (SelectedChiTietDatBanHomNay != null)
                {
                    
                    return true;
                }   
                else return false;
            }, (p) =>
            {
                var y = DataProvider.Ins.DB.DsChiTietDatBan.Where(q => q.IdKhachHang == SelectedChiTietDatBanHomNay.IdKhachHang && q.NgayDat == SelectedChiTietDatBanHomNay.NgayDat).SingleOrDefault();
                var k = DataProvider.Ins.DB.DsKhachHang.Where(q => q.IdKhachHang == SelectedChiTietDatBanHomNay.IdKhachHang).SingleOrDefault();
                string mess = "Bạn có muốn hủy đơn đặt bàn của " + k.TenKhachHang + " lúc " + SelectedChiTietDatBanHomNay.NgayDat;   
                if(SelectedTinhTrang.TenTinhTrang == "Hủy")
                {
                    if (MessageBox.Show(mess, "Thong báo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        y.TrangThai = "Hủy";
                        ChiTietDatBanHomNay.Remove(SelectedChiTietDatBanHomNay);
                        ChiTietDatBan.Remove(SelectedChiTietDatBanHomNay);
                    }
                }
                else if (SelectedTinhTrang.TenTinhTrang == "Hoàn thành")
                {
                    ChiTietDatBan.Remove(SelectedChiTietDatBanHomNay);
                    ChiTietDatBanHomNay.Remove(SelectedChiTietDatBanHomNay);
                    y.TrangThai = SelectedTinhTrang.TenTinhTrang;
                }
                DataProvider.Ins.DB.SaveChanges();
            });
        }
    }
}
