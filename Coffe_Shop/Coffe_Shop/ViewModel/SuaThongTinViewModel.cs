﻿using Coffe_Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Coffe_Shop.ViewModel
{
    public class SuaThongTinViewModel : BaseViewModel
    {
        private NguoiDung _NguoiDung;
        public NguoiDung NguoiDung { get => _NguoiDung; set { _NguoiDung = value; OnPropertyChanged(); } }
        private string _TenDangNhap;
        public string TenDangNhap { get => _TenDangNhap; set { _TenDangNhap = value; OnPropertyChanged(); } }
        private string _MatKhauCu;
        public string MatKhauCu { get => _MatKhauCu; set { _MatKhauCu = value; OnPropertyChanged(); } }
        private string _MatKhauMoi;
        public string MatKhauMoi { get => _MatKhauMoi; set { _MatKhauMoi = value; OnPropertyChanged(); } }
        public ICommand LoadedWindowCommand { get; set; } //Bài Coffe Shop
        public ICommand SaveCommand { get; set; } //Bài Coffe Shop
        public SuaThongTinViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                TenDangNhap = NguoiDung.IdNguoiDung;
            });
            SaveCommand = new RelayCommand<Window>((p) => 
            {
                if (MatKhauCu == "" || MatKhauMoi == "")
                    return false;
                else return true;
            }, (p) => 
            {
                if(MatKhauCu != NguoiDung.MatKhau)
                {
                    MessageBox.Show("Mật khẩu cũ không đúng", "Cảnh báo", MessageBoxButton.OK, MessageBoxImage.Error);
                    MatKhauCu = "";
                    MatKhauMoi = "";
                }
                else
                {
                    var x = DataProvider.Ins.DB.DsNguoiDung.Where(q => q.IdNguoiDung == NguoiDung.IdNguoiDung).SingleOrDefault();
                    x.MatKhau = MatKhauMoi;
                    DataProvider.Ins.DB.SaveChanges();
                    MatKhauCu = "";
                    MatKhauMoi = "";
                }
            });
        }
    }
}
