﻿using Coffe_Shop.Model;
using Coffe_Shop.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coffe_Shop.View
{
    public class ViewChonMon: BaseViewModel
    {
        private ThucDon _ThucDon;
        public ThucDon ThucDon { get => _ThucDon; set { _ThucDon = value; OnPropertyChanged(); } }
        private int _SoLuong;
        public int SoLuong { get => _SoLuong; set { _SoLuong = value; OnPropertyChanged(); } }
        private int _TongTien;
        public int TongTien { get => _TongTien; set { _TongTien = value; OnPropertyChanged(); } }
        private bool _DaChon;
        public bool DaChon { get => _DaChon; set { _DaChon = value; OnPropertyChanged(); } }
    }
}
